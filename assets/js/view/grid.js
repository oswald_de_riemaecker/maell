if (! window.maell) {
	window.maell = [];
}
if (! window.maell.view) {
	window.maell.view = [];
}

if (! window.maell.view.grid) {
	
	/**
	 * Grid object, display a server-side collection
	 * @param param
	 * @returns {maell.view.grid}
	 */
	window.maell.view.grid = function(param, parentNode) {

		this.obj = param;
		
		this.collection = new maell.object.collection(param.obj);
		
		this.params = param.params;
		
		this.display = param.display;
		
		this.events = param.events;
		
		this.callbacks = {remove:null,before:{remove:null},after:{remove:null}};
		
		this.table;
		
		
		this.init = function() {
			for (var i in this.events.row) {
				switch (this.events.row[i].callback) {
				case '%grid.delete%':
					this.events.row[i].callback = jQuery.proxy(this, 'deleteRow');
					this.events.row[i].options  = {icon:'delete',size:'small',nolabel:true};
					break;
					
				case '%grid.update%':
					this.events.row[i].callback = jQuery.proxy(this, 'updateRow');
					this.events.row[i].options  = {icon:'tool-blue',size:'small',nolabel:true};
					break;					
				}
				
			};
		};
		
		
		this.render = function(dom) {
			this.table = new maell.view.table({id:'grid',display:this.display,events:this.events,collection:this.params.collection});
			this.table.render(dom);
			
			if (true) { // enable sorting
				jQuery('#lignes tbody tr').droppable({
					helper: function(event) {
						return jQuery('<div style="width:100%"><table></table></div>')
							.find('table').append(jQuery(event.target).closest('tr').clone()).end();
						},
				});
				jQuery('#grid tbody').sortable({cursor:'move'});
			}
		};
		
		
		this.populate = function(obj) {
			if (obj && obj.status) {
				this.table.addRow(obj.data.members);
				return;
			}
			maell.core.call({action:'grid/populate', data:{uuid:this.obj.uuid}, callback:jQuery.proxy(this,'populate')});
		};
		
		
		this.updateRow = function(obj) {
			console.log(obj);
		};
		
		
		this.deleteRow = function(obj) {
			maell.object.remove(obj);
		};
		
		
		this.init();
		this.render('elem_' + parentNode);
		this.populate();
	};
	
	
	window.maell.view.grid.column = function(id,title,type) {
		this.id = id;
		this.title = title;
		this.type = type;
	};
}
