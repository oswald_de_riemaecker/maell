if (! window.maell) {
	window.maell = [];
}
if (! window.maell.view) {
	window.maell.view = [];
}

/**
 * build an informational box and (default behaviour) display it
 * to prevent immediate display, set maell.view.alert.defaults.autorun to false
 * 
 * 
 * @param str message to display
 * @param o parameters
 * @returns {window.maell.view.alert}
 */
window.maell.view.map = function(obj, options, type) {

	this.type = type || 'google';
	
	this.map, this.geocoder, this.markers;
	
	
	this.codeAddress = function(str, center) {
		if (! this.geocoder) {
			this.geocoder = new google.maps.Geocoder();
		}
		map = this.map;
		center = center || false;
		this.geocoder.geocode({address:str}, function(results, status) {
			 if (status == google.maps.GeocoderStatus.OK) {
				 if (center) map.setCenter(results[0].geometry.location);
				 new google.maps.Marker({
			          map: map,
			          position: results[0].geometry.location
			      });
			    } else {
			      alert('Geocode was not successful for the following reason: ' + status);
			    }
		});
	};
	
	
	this.codeAddresses = function(locations) {
		for (var i in locations) {
			this.codeAddress(locations[i], (i == 0));
		}
	};
	
	
	
	switch (this.type) {
		default:
			this.map = new google.maps.Map(document.getElementById(obj.id + '_map'), options);
			this.codeAddresses(obj.locations);
			break;
	}
};

