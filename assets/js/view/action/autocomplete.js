if (! window.maell) {
	window.maell = [];
}
if (! window.maell.view) {
	window.maell.view = [];
}

if (! window.maell.view.action) {
	window.maell.view.action = {registry:{}};
}

if (! window.maell.view.action.autocomplete) {

	/**
	 * Autocompleter object
	 * @param obj
	 * @param element
	 * @returns {window.maell.view.action.autocomplete}
	 */
	window.maell.view.action.autocomplete = function(obj,element) {
	
		/*
		 * Ordered collection of properties to display
		 */
		this.display = obj.data.display;
		
		this.sdisplay = obj.data.sdisplay;
		
		// real (hidden) field target
		this.target = obj.data.target;

		/**
		 * @deprecated use this.options.minChars instead
		 */
		this.minChars = obj.data.minChars;
		
		this.options = obj.data;
		
		/*
		 * Number of the last server call
		 * We only accept to handle the one response holding this sequence number
		 */
		this.sequence = 0;
		
		/**
		 * @deprecated use this.options.displayMode instead
		 */
		this.displayMode = obj.data.displayMode;
		
		this.previous, this.observer, this.history = {}, this.nextQuery;
		
		// callbacks
		this.callbacks = obj.callbacks || {};
		
		if (! this.callbacks['display']) this.callbacks['display'] = maell.view.action.autocomplete.display;
		if (! this.callbacks['select'])  this.callbacks['select']  = ''; //'maell.view.action.autocomplete.select';
		if (! this.callbacks['extend'])  this.callbacks['extend']  = 'maell.view.action.autocomplete.extendSuggestions';

		
		// server-side action uuid 
		this.uuid = obj.data.uuid;

		// DOM element where autocomplete action occurs
		this.element = jQuery('#' + element);
		this.props = element + '_acprops';
		
		// Server Uri
		this.action = obj.data.action || 'action/autocomplete';
		
		this.currentSuggestions = {};
		
		this.currentId;
		
		
		this.init = function() {
			// add accessories DOM elements
			var span = document.createElement('SPAN');
			span.setAttribute('id', this.target + '_display');
			span.setAttribute('title', 'cliquez pour modifier');
			maell.view.bindLocal(span, 'click', maell.view.action.autocomplete.reset, this);
			this.element.after(span);
			
			// if a value exists, transform it
			if ((val = jQuery('#' + this.target).val()) != '') {
				this.initSavedValue(val);
			}
			
			// start observer (tested with IE 11)
			this.observer = window.setInterval(maell.view.action.autocomplete.observer, 100, this);
			this.element.focus();
		};
		
		
		/**
		 * Detect & convert initial existing value
		 */
		this.initSavedValue = function(val) {
			var config = {
							action:this.action,
							callback:jQuery.proxy(this, 'displaySavedValue'),
							data:{
									_id:val,
									uuid:this.uuid
								 }
					 };
			maell.core.call(config);
		};
		
		
		this.displaySavedValue = function(obj) {
			if (obj.data && obj.data._id) {
				var val = obj.data.collection[obj.data._id];
				this.setValue(obj.data._id, this.prepareDisplay(val));
			}
		};
		
		
		/**
		 * Display suggestions collection
		 */
		this.displaySuggestions = function(data) {
			
			this.currentSuggestions = data.collection;
			
			var size = 0;
			for (var id in this.currentSuggestions) {
				if (this.currentSuggestions.hasOwnProperty(id)) size++;
			}
			
			if (size == 1 && this.options.defaultSelect == true) {
				// display directly unique value returned by query
				for (var id in this.currentSuggestions) {
					this.defaultSelect(id);
					return true;
				}
			}
			
			switch (this.options.displayMode) {
			
				case 'table':
					this._displayAsTable();
					break;
					
				case 'list':
					this._displayAsList();
					break;
			}

			jQuery('#'+this.props).remove();
			jQuery('<div></div>').addClass('maell component autocompletepropsgrid').attr('id', this.props).appendTo('body');

			var helper = this.getHelper(data);
			var helpertext = helper.txt;
			var css = helper.css;
			var id = this.props;
			var element = this.element.attr('id');

			jQuery('<h4>').appendTo(jQuery('#'+this.props)).html(helpertext).attr('id', id+'_helper').addClass(css);

			if (css == 'more') {
				var button = new maell.view.button(maell.lget('ac:extend'), {icon:'more-blue',css:'ac_extend',id:id+'_extend'});
				jQuery('#' + this.props).append(button);
			}

			jQuery(document).bind('click.propsgrid', function(e) {
				if (e.target.id == id+'_extend' || jQuery(e.srcElement).parent().attr('id') == id+'_extend') {
					maell.view.action.autocomplete.extender(maell.view.registry[element]);
				} else if (e.target.id != id) {
					jQuery('#'+id).remove();
					jQuery(document).unbind('click.propsgrid');
					maell.view.registry[element].offset = 0;
				}
			});

			if (data.total > 0) {
				this.table.render(jQuery('#' + this.props));
				if (this.callbacks.select == '') {
					var callback = jQuery.proxy(this,'select');
				} else if (typeof this.callbacks.select == 'function') {
					var callback = this.callbacks.select;
				} else {
					var callback = eval(this.callbacks.select);
				}
				maell.view.bindLocal(jQuery('#' + this.props + '_table'), 'click', callback, this);
			}
			this.refreshSuggestionsPosition();
			this.element.focus();
		};

		
		/**
		 * Triggered action when a value is clicked
		 * Receives the current autocomplete object as context
		 */
		this.select = function(obj) {
			this_currentId = '';
			switch (this.displayMode) {
				case 'table':
					this.currentId = jQuery(obj.target).parent('tr').data('id');
					break;
					
				case 'list':
					this.currentId = jQuery(obj.target).parent('tr').data('id');
					break;
			}
			
			if (this.currentId) {
				this.defaultSelect(this.currentId);
			} else {
				console.log('selected row id is missing');
			}
		};
		
		this.getHelper = function(data) {
			if (data.total == 0 || ! data.total) {
				var helpertext = maell.lget('ac:noresult');
				var css = 'none';
			} else if (data.total == 1) {
				var helpertext = maell.lget('ac:oneresult');
				var css = 'one';
			} else if (data.max > data.total) {
				var helpertext = maell.lget('ac:moreresults', {vars:[data.total, data.max]});
				var css = 'more';
			} else {
				var helpertext = maell.lget('ac:manyresults', {vars:[data.total]});
				var css = 'many';
			}
			return {txt:helpertext, css:css};
		};

		
		this.refreshSuggestionsPosition = function() {
			var grid = jQuery('#' + this.props);
			jQuery(document).unbind('resize.propsgrid');
			var offset = this.element.offset();
			var height = this.element.height()+4;
			var top = parseInt(offset.top) + parseInt(height) + 5 + 'px';
			var left = parseInt(offset.left) + 'px';
			grid.css({position:'absolute', top:top, left:left});
			
			// refresh position on document resize (broken ATM)
			jQuery(document).bind('resize.propsgrid', function(){
				maell.view.autocomplete.refreshSuggestionsPosition(input, grid);
			});
		};

		
		this._displayAsTable = function() {
			this.table = new maell.view.table({
				'display': jQuery(this.sdisplay).length > 0 ? this.sdisplay : this.display,
				'collection': new maell.object.collection({collection:this.currentSuggestions}),
				'id': this.props+'_table'
			});
		};
		
		
		this._displayAsList = function() {
			this._displayAsTable();
		};
		
		
		this.defaultSelect = function(id) {
			if (! this.currentSuggestions[id]) {
				console.log('Missing member for key ' + id);
			}
			
			this.resetSuggestions();
			var selected = this.currentSuggestions[id];
			this.currentId = id;
			this.setValue(id, this.prepareDisplay(selected));
			if (this.callbacks.postSelect && typeof this.callbacks.postSelect == 'function') {
				this.callbacks.postSelect.call(this,selected,id);
			}
		};
		
		
		this.prepareDisplay = function(obj) {
			if (obj == undefined) {
				return '';
			}
			var label = '';
			var obj = new maell.object.factory(obj);
			for (var i in this.display) {
				if (label.length > 0) label += ' ';
				label += obj.get(i);
			}
			return label || obj.value;
		};
		
		
		this.setValue = function(key, label) {
			this.element.hide();
			var width = this.element.width()+5;
			var display = jQuery('#' + this.target + '_display').css({
				display: 'inline-block',
				cursor: 'pointer',
				'text-align':'left',
				'padding-top':'5px'
			});
			display.html(label);
			jQuery('#' + this.target).val(key);
			this.triggerChangeEvent();
		};
		
		
		this.resetValue = function() {
			var display = jQuery('#' + this.target + '_display');
			display.hide();
			jQuery('#' + this.target).val('_NONE_');
			this.resetSuggestions();
			this.offset = 0;
			if (this.callbacks.postReset && typeof this.callbacks.postReset == 'function') {
				this.callbacks.postReset.call(this);
			}
			this.triggerChangeEvent();
		};
		
		
		this.resetSuggestions = function() {
			jQuery('#'+this.props).remove();
			this.element.val('').show().focus();
			this.previous = null;
		};

		
		/**
		 * trigger a 'change' event upon value setting so external observers can catch it
		 */
		this.triggerChangeEvent = function() {
			maell.view.customEvent(this.target, 'change');
		};
	};

	
	window.maell.view.action.autocomplete.extendSuggestions = function(obj) {

		switch (obj.status) {
		
			case maell.core.status.ok:
				var ac = maell.view.registry[this.attr('id')];
				
				// ignore queries coming back home too late
				if (ac.sequence > obj.data._sequence) {
					console.log('ignored server late response #' + obj.data._sequence + ' for query "' + obj.data._query + '".');
					return false;
				}
				
				// abort if there are no new results 
				if (ac.offset >= obj.data.max || obj.data.collection.length==0) {
					return false;
				}

				// store the new set of results
				for (d in obj.data.collection) {
					ac.currentSuggestions[d] = obj.data.collection[d];
				}

				var element = this.attr('id');
				var table = jQuery('#'+maell.view.registry[element].table.id);

				// add some space for scrollbar, only once
				if (!table.hasClass('scroll')) {
					table.width(table.width()+20);
					table.height(table.height()+40);
					jQuery('a#'+element+'_acprops_extend.icon').css({
						'margin-right': 30
					});
				}
				table.addClass('scroll');

				// update or set offset value
				if (ac.offset) {
					ac.offset += obj.data.total;
				} else {
					ac.offset = 10;
				}

				// push collection into the table
				maell.view.registry[element].table.addRow(
					obj.data.collection,
					{'repeatheaders': true}
				);

				// update helper text
				var data = obj.data;
				data.total = ac.offset;
				var helper = ac.getHelper(data);
				jQuery('#'+ac.props+'_helper').html(helper.txt).removeClass('none one many more').addClass(helper.css);

				// remove the 'extend results' link?
				if (helper.css!='more') {
					jQuery('#'+ac.props+'_extend').remove();
				}

				// scroll down to show latests results
				if (true) {
					maell.view.scroll(jQuery('#'+ac.props + ' tbody'), {'more': 80});
				}

				break;
			
			default:
				console.log(obj);
				break;
		}

	};


	window.maell.view.action.autocomplete.extender = function(obj) {

		var search = jQuery(obj.element.selector).val();
		var ac = obj;

		if (!ac.offset) {
			ac.offset = 0;
		}
		var offset = ac.offset;
		
		var config = {
						action:ac.action,
						callback:eval( ac.callbacks['extend'] ),//maell.view.registry[ jQuery(ac.element).attr('id') ].callbacks['extend'],
					 	context:ac.element,
						data:{_query:search,uuid:ac.uuid,_sequence:++ac.sequence, _offset:offset}
					 };
		maell.core.call(config);
	};
	
	
	/**
	 * Autocompleter Observer
	 * @param obj
	 */
	window.maell.view.action.autocomplete.observer = function(obj) {

		var search = obj.element.val().trim();	
		if (search.length < obj.minChars || search == obj.previous) {
			return;
		}

		if (obj.nextQuery) { // cancel previous call
			window.clearTimeout(obj.nextQuery);
			obj.nextQuery = null;
		}
		
		obj.previous = search;
		obj.element.val(search);
		
		if (obj.history[search]) {
			obj.callbacks.display.call(obj,obj.history[search], 0);
			return;
		}
		
		var config = {
						action:obj.action,
						callback:function(res) { obj.callbacks.display.call(obj,res) },
					 	context:this.element,
						data:{_query:search,uuid:obj.uuid,_sequence:++obj.sequence,_offset:0}
					 };
		
		if (obj.callbacks.preQuery && typeof obj.callbacks.preQuery == 'function') {
			config = obj.callbacks.preQuery.call(obj,config);
		}
		
		obj.nextQuery = window.setTimeout(function() {
			maell.view.shade(obj.element);
			maell.core.call(config);
		}, obj.options.latency);
	};
	
	
	/**
	 * Default autocompleter Ajax-success Handler
	 * restore the autocomplete object and call the displaySuggestions() methods with the received collection
	 * @param obj
	 * @param offset
	 * @param this Current autocomplete object
	 */
	window.maell.view.action.autocomplete.display = function(obj, offset) {

		offset = isNaN(offset) ? false : offset;
		if (offset !== false) {
			obj = obj[offset];
			console.log('"' + obj.data._query + '" query result from local cache');
		} else {
			if (! this.history[obj.data._query]) this.history[obj.data._query] = {};
			this.history[obj.data._query][obj.data._offset] = obj;
			maell.view.shade(this.element);
		}
		
		switch (obj.status) {
			case maell.core.status.ok:
			case maell.core.status.nok:
				// ignore server queries coming back too late
				if (offset === false && this.sequence > obj.data._sequence) {
					return false;
				}
				this.displaySuggestions(obj.data);
				this.offset = obj.data.total;
				break;

			case maell.core.status.err:
				new maell.view.alert(maell.lget('err:bkd'),	{title: maell.lget('err:lbl_srv'), autorun:true, level:'srv_error'});
				break;
			
			default:
				console.log(obj);
				break;
		}
	};

	
	window.maell.view.action.autocomplete.reset = function(obj) {
		var ac = obj.data.caller;
		ac.resetValue();
	};
}
