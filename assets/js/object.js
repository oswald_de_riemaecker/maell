if (! window.maell) {
	window.maell = [];
}

if (! window.maell.object) {

	
	/**
	 * Returns a new instance of a maell.object.base object
	 * @param data
	 * @returns {maell.object.base}
	 * @deprecated use maell.object.factory
	 */
	window.maell.object = function(data) {
		var obj = new maell.object.base();
		if (data) {
			obj.uuid = data.uuid;
			obj.data = new maell.object.data(data.props);
			obj.params = maell.core.setParameters(data.params);
		}
		return obj;
	};

	
	/**
	 * Returns a new instance of a maell.object.base object
	 * @param data
	 * @returns {maell.object.base}
	 */	
	window.maell.object.factory = function(data) {
		
		var obj = new maell.object.base();
		if (data) {
			obj.uuid = data.uuid;
			obj.data = new maell.object.data(data.props);
			obj.params = maell.core.setParameters(data.params);
		}
		return obj;		
	};
	
	
	/**
	 * Simple ObjectModel\DataObject representation
	 * @param properties
	 * @returns {window.maell.object.data}
	 */
	window.maell.object.data = function(properties) {
	
		this.get = function(key,formatted) {
		
			try {
				return this.properties[key].get(formatted);
			} catch (e) {
				console.log('undefined property ' + key);
			}
		};
		
		this.getProperty = function(key) {
			try {
				return this.properties[key];
			} catch (e) {
				console.log('undefined property ' + key);
			}
		};
	
		this.set = function(key,val) {
		
			this.properties[key].val = val;
			return this;
		};
		
		this.setProperties = function(data) {
		
			this.properties = {};
			for (var i in data) {
			
				//console.log(data[i]);
				this.properties[i] = new maell.object.data.property(data[i].type, 
																  data[i].uuid ? data[i] : data[i].value,
																  data[i].label,
																  data[i].params
																 );
				
				// register object display value
				if (data[i].uuid && typeof data[i].value == 'string') {
					this.properties[i].display = data[i].value;
				}
				
				// @todo add values, contraints, etc.
			}
		};
		
		this.toArray = function(selected) {
			
			var array = {};
			for (var i in this.properties) {
				if (selected && selected.indexOf(i) == -1) continue;
				array[i] = this.properties[i].val;
			}
			
			return array;
		};
		
		// constructor
		this.setProperties(properties);
	};
	

	/**
	 * Property handler
	 */
	window.maell.object.data.property = function(type, val, label, params) {
		
		// store display value (for objects)
		this.display;
		
		this.params = params || [];
		
		this.get = function(formatted) {
			
			var val = this.val;
			if (this.params.constraints && this.params.constraints.multilingual != undefined) {
				val = val[maell.locale.lang] ? val[maell.locale.lang] : val['__'];
			}
			switch (this.type) {
			
				case 'Object':
					return this.display || this.val;
					break;
					
				case 'Currency':
					return !formatted || formatted == false ? val : maell.view.format(val,'Currency');
					break;
					
				case 'Date':
					return !formatted || formatted == false ? val : maell.view.format(val,'Date');
					break;
					
				default:
					return val;
					break;
			}
		};
		
		
		this.set = function(val) {
			
			switch (this.type) {
			
				case 'Object':
					//console.log(val);
					if (val && val.value && val.value.props && ! val.props) {
						val.props = val.value.props;
						val.value = null;
					}
					val = new maell.object(val);
					break;
				
				case 'Collection':
					var c = new maell.object.collection();
					c.populate(val);
					val = c;
					break;
			}
			
			this.val = val;
			return this;
		};
		
		
		// constructor
		this.type = type;
		this.label = label;
		this.set(val);
	};
	
	
	/* base object instance */
	window.maell.object.base = function () {
		
		this.data,this.params,this.uuid;
		
		this.get = function(key, formatted) {
			return this.data && typeof this.data.get(key) != 'undefined' ? this.data.get(key, formatted): null;
		};
		
		this.getProperty = function(key) {
			return this.data && this.data.getProperty(key) ? this.data.getProperty(key): null;
		};
		
		this.set = function(key,val) {
			return this.data.set(key,val);
		};
		
		this.save = function() {
			maell.core.ajax(maell.core.backend + '/object/save', {uuid:this.uuid, data:this.data.toArray()});
		};
	};
	
	
	/**
	 * This object represents a Maell\ObjectModel\Collection
	 * @param param may be either a reduced collection or an uuid 
	 * @returns {maell.object.collection}
	 */
	window.maell.object.collection = function(param) {
		
		this.members = {};
		
		/**
		 * Execute a query on backend
		 */
		this.find = function() {};
		
		
		this.getMember = function(id) {
			return this.members[id] || false;
		};
		
		
		/**
		 * Populate collection with given json-originated object
		 */
		this.populate = function(obj) {
			var c = {};
			jQuery.each(obj, function(i,o) { c[i] = new maell.object(o); });
			this.members = c;
		};
		
		// instanciation
		if (typeof param == 'object') {
			this.populate(param['collection']);
			this.uuid = param.uuid;
		} else {
			this.uuid = param;
		}
	};
	
	
	window.maell.object.edit = function() {
		var src = maell.view.caller;
		var row = jQuery(src).closest('tr');
		var cols = row.find('td');
		
		maell.view._data = {uuid:src.data('uuid'), alias:src.data('alias'), id:src.data('id')};
		maell.view._currentDomElem = jQuery(maell.view.caller).closest('tr');
		
		maell.core.call({
			action:'collection/edit', 
			data:maell.view._data
		 });
	};
	
	
	window.maell.object.remove = function(src) {
		var src = jQuery(src);
		maell.view._data = {uuid:src.data('uuid'), alias:src.data('alias'), id:src.data('id')};
		maell.view._currentDomElem = src.closest('tr');
		maell.view._alert = maell.view.alert.confirm(maell.lget('confirm:remove'),{confirm:maell.object.retDelete});
	};
	
	
	window.maell.object.retDelete = function(obj) {
		if (obj && obj.status) {
			switch (obj.status) {
				case maell.core.status.ok:
					jQuery(maell.view._currentDomElem).remove();
					break;
				
				default:
					new maell.view.alert(maell.lget('err:remove'));
					break;
			}
			return;
		}
		maell.view._alert.hide();
		maell.core.call({
						action:'collection/delete', 
						data:maell.view._data
					 });
	};
}
