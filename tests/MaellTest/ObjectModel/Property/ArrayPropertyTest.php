<?php

namespace MaellTest\ObjectModel\Property;

use Maell\ObjectModel\Property\ArrayProperty;
use Maell\Core\Locale;

/**
 * StringProperty test case.
 */
class ArrayPropertyTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var ArrayProperty
     */
    private $ArrayProperty;
    
    
    /**
     * Shared Fixture
     * @var array
     */
    private $array;

    
    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->ArrayProperty = new ArrayProperty('test');
        $this->array = ['one','two', 'two' => 'three'];
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        $this->ArrayProperty = null;
        parent::tearDown();
    }

    
    /**
     * Tests ArrayProperty->setValue() with an array
     */
    public function testSetValueFromArray()
    {
        $this->ArrayProperty->setValue($this->array);
        $this->assertEquals($this->array, $this->ArrayProperty->getValue());
    }
    
    
    /**
     * Tests ArrayProperty->setValue() with a serialized string
     */
    public function testSetValueFromSerializedString()
    {
        $this->ArrayProperty->setValue(serialize($this->array));
        $this->assertEquals($this->array, $this->ArrayProperty->getValue());
    }
    

    /**
     * Tests ArrayProperty->setValue() with a string
     * @expectedException Maell\ObjectModel\Property\Exception
     */
    public function testSetValueFromString()
    {
        $this->ArrayProperty->setValue('a string');
    }
    
    
    /**
     * Tests ArrayProperty->getValue() with a numeric key
     */
    public function testGetValueWithNumericKey()
    {
        $this->ArrayProperty->setValue($this->array);
        $this->assertEquals('two', $this->ArrayProperty->getValue(1));
    }
    
    
    /**
     * Tests ArrayProperty->getValue() with an alphanumeric key
     */
    public function testGetValueWithAlphanumericKey()
    {
        $this->ArrayProperty->setValue($this->array);
        $this->assertEquals('three', $this->ArrayProperty->getValue('two'));
    }
    
    
    /**
     * Tests ArrayProperty->getDisplayValue()
     */
    public function testGetDisplayValue()
    {
        $this->ArrayProperty->setValue($this->array);
        $this->assertEquals('one,two,three', $this->ArrayProperty->getDisplayValue());
    }
    
    
    /**
     * Tests ArrayProperty->getDisplayValue() with multilingual option
     */
    public function testGetDisplayValueWithMultilingualModeEnabled()
    {
        $this->ArrayProperty->setParameter('constraints.multilingual', true);
        $this->ArrayProperty->setValue(['en' => 'English', 'fr' => 'Français']);
        
        Locale::$lang = 'en';
        $this->assertEquals('English', $this->ArrayProperty->getDisplayValue());

        Locale::$lang = 'fr';
        $this->assertEquals('Français', $this->ArrayProperty->getDisplayValue());
    }    
}
