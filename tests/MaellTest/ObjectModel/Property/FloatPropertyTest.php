<?php

namespace MaellTest\ObjectModel\Property;

use Maell\ObjectModel\Property\FloatProperty;

/**
 * StringProperty test case.
 */
class FloatPropertyTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var FloatProperty
     */
    private $FloatProperty;
    
    
    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->FloatProperty = new FloatProperty('test');
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        $this->FloatProperty = null;
        parent::tearDown();
    }

    
    /**
     * Tests FloatProperty->setValue() with an Float
     */
    public function testSetValueWithFloat()
    {
        $this->FloatProperty->setValue(123.45);
        $this->assertEquals(123.45, $this->FloatProperty->getValue());
    }
    

    /**
     * Tests FloatProperty->setValue() with a numeric string
     */
    public function testSetValueWithNumericString()
    {
        $this->FloatProperty->setValue("123.45");
        $this->assertEquals(123.45, $this->FloatProperty->getValue());
    }
    
    
    /**
     * Tests FloatProperty->setValue() with a non numeric string
     * @expectedException Maell\ObjectModel\Property\Exception
     */
    public function testSetValueWithNonNumericString()
    {
        $this->FloatProperty->setValue("string");
    }
    
    
    /**
     * Tests StringProperty->getDisplayValue()
     */
    public function testGetDisplayValue()
    {
        $this->FloatProperty->setValue(5100.5112);
        $this->assertEquals('5 100,51', $this->FloatProperty->getDisplayValue());
    }
    
    
    /**
     * Tests StringProperty->getDisplayValue()
     */
    public function testGetDisplayValueWithParameterPrecisionSet()
    {
        $this->FloatProperty->setParameter('constraints.precision', 4);
        $this->FloatProperty->setValue(5100.5112);
        $this->assertEquals('5 100,5112', $this->FloatProperty->getDisplayValue());
    }
}
