<?php

namespace MaellTest\ObjectModel\Property;

use Maell\ObjectModel\Property\CurrencyProperty;

/**
 * StringProperty test case.
 */
class CurrencyPropertyTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var CurrencyProperty
     */
    private $CurrencyProperty;
    
    
    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->CurrencyProperty = new CurrencyProperty('test', ['currency' => 'fr_FR.UTF-8']);
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        $this->CurrencyProperty = null;
        parent::tearDown();
    }

    
    /**
     * Tests CurrencyProperty->setValue() with different values
     */
    public function testSetValueFromString()
    {
        $this->CurrencyProperty->setValue(100);
        $this->assertEquals(100, $this->CurrencyProperty->getValue());
    }
    

    /**
     * Tests CurrencyProperty->setValue() with an incorrect string
     */
    public function testSetValueFromIncorrectString()
    {
        $this->setExpectedException('Maell\ObjectModel\Property\Exception');
        $this->CurrencyProperty->setValue('one hundred and two');
    }
    
    
    
    /**
     * Tests StringProperty->getDisplayValue()
     */
    public function testGetDisplayValue()
    {
        $this->CurrencyProperty->setValue(5100.5112);
        $this->assertEquals('5 100,51 €', $this->CurrencyProperty->getDisplayValue());
    }
    
    
    /**
     * Tests StringProperty->getDisplayValue()
     */
    public function testGetDisplayValueWithParameterPrecisionSet()
    {
        $this->CurrencyProperty->setParameter('constraints.precision', 4);
        $this->CurrencyProperty->setValue(5100.5112);
        $this->assertEquals('5 100,5112 €', $this->CurrencyProperty->getDisplayValue());
    }
}
