<?php

namespace MaellTest\ObjectModel\Property;

use Maell\ObjectModel\Property\StringProperty;

/**
 * StringProperty test case.
 */
class StringPropertyTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var StringProperty
     */
    private $StringProperty;

    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->StringProperty = new StringProperty('test');
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        $this->StringProperty = null;
        parent::tearDown();
    }
    
    
    /**
     * @expectedException Maell\ObjectModel\Property\Exception
     */
    public function testSetValueWithEverythingButAString()
    {
        $this->StringProperty->setValue(3);
        $this->StringProperty->setValue([]);
    }

    
    /**
     * Tests StringProperty->setValue() with uppercase transformation
     */
    public function testSetValueWithUppercaseConstraintEnabled()
    {
        $property = clone $this->StringProperty;
        $property->setParameter('constraints.uppercase', true);
        $property->setValue('string');
        $this->assertEquals('STRING', $property->getValue());
    }

    /**
     * Tests StringProperty->setValue() with lowercase transformation
     */
    public function testSetValueWithLowercaseConstraintEnabled()
    {
        $property = clone $this->StringProperty;
        $property->setParameter('constraints.lowercase', true);
        $property->setValue('STRING');
        $this->assertEquals('string', $property->getValue());
    }
    
   
    /**
     * Tests StringProperty->setValue() with minlength constraint
     */
    public function testSetValueWithMinLengthConstraintEnabled()
    {
        $this->setExpectedException('Maell\ObjectModel\Property\Exception');
        $property = clone $this->StringProperty;
        $property->setParameter('constraints.minlength', 5);
        $property->setValue('stri');
    }
    
    
    /**
     * Tests StringProperty->setValue() with maxlength constraint
     */
    public function testSetValueWithMaxLengthConstraintEnabled()
    {
        $this->setExpectedException('Maell\ObjectModel\Property\Exception');
        $property = clone $this->StringProperty;
        $property->setParameter('constraints.maxlength', 4);
        $property->setValue('string');
    }
}

