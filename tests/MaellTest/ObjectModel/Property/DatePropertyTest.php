<?php

namespace MaellTest\ObjectModel\Property;

use Maell\ObjectModel\Property\DateProperty;

/**
 * StringProperty test case.
 */
class DatePropertyTest extends \PHPUnit_Framework_TestCase
{
    /**
     *
     * @var DateProperty
     */
    private $DateProperty;
    
    
    private $today;
    
        
    /**
     * Prepares the environment before running a test.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->DateProperty = new DateProperty('test', ['format' => 'yyyy-MM-dd']);
        $this->today = date('Y-m-d H:i:s');
    }

    /**
     * Cleans up the environment after running a test.
     */
    protected function tearDown()
    {
        $this->DateProperty = null;
        parent::tearDown();
    }

    
    /**
     * Tests StringProperty->setValue() with a string
     */
    public function testSetValueFromString()
    {
        $this->DateProperty->setValue($this->today);
        $this->assertEquals($this->today, $this->DateProperty->getValue());
    }
    

    /**
     * Tests StringProperty->setValue() with an incorrect string
     * @expectedException Maell\ObjectModel\Property\Exception
     */
    public function testSetValueFromIncorrectString()
    {
        $this->DateProperty->setValue('2015-02-29');
    }
    
    
    /**
     * Tests StringProperty->setValue() with class constants
     */
    public function testSetValueFromConstants()
    {
        $parts =explode(' ', $this->today);
        
        $this->DateProperty->setValue(DateProperty::TODAY);
        $this->assertEquals($this->today, $this->DateProperty->getValue());

        $this->DateProperty->setValue(DateProperty::TODAY_DATE);
        $this->assertEquals($parts[0], $this->DateProperty->getValue());
        
        $this->DateProperty->setValue(DateProperty::TODAY_TIME);
        $this->assertEquals($parts[1], $this->DateProperty->getValue());
        
        $this->DateProperty->setValue(DateProperty::TOMORROW);
        $this->assertEquals(date('Y-m-d H:i:s', time()+86400), $this->DateProperty->getValue());
    }
    
    
    /**
     * Tests StringProperty->getDisplayValue()
     */
    public function testGetDisplayValue()
    {
        $this->DateProperty->setValue(DateProperty::TODAY);
        $this->assertEquals(date('d/m/Y'), $this->DateProperty->getDisplayValue());
        $this->assertEquals(date('d/m/Y H\hi'), $this->DateProperty->getDisplayValue(true));
        
    }
}
