<?php

use Maell as Maell;

require_once '../library/Maell.php';


$path = substr(__DIR__, 0, strrpos(__DIR__, DIRECTORY_SEPARATOR)+1);

$loader = require $path . 'vendor/autoload.php';
$loader->add("Maell\\", $path . 'library/');
$loader->add("MaellTest\\", $path . 'tests/');

Maell::$basePath = Maell::$maellPath = $path;
Maell::init(Maell::ENV_TEST);

