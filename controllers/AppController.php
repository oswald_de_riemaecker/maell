<?php

/**
 * DefaultController
 * 
 * @author
 * @version 
 */

require_once 'AssetsController.php';

class Maell_AppController extends Maell_AssetsController {

	
	public function __call($methodName, $args)
	{
		if ($this->_getParam('action')) {
			$params = array_slice($this->_getAllParams(),3,1);
			if (count($params) == 0) {
				die("incorrect url format");
			}
			
			$basepath = \Maell::$basePath;
			if (substr($basepath, -1) == DIRECTORY_SEPARATOR) {
			    $basepath = substr($basepath,0, strlen($basepath)-1);
			}
			$segments = array(
								$basepath, 
								'application',
								'modules', 
								$this->_getParam('action'), 
								implode(array_keys($params)),
								'assets',
							 );
			
			$filepath = implode(DIRECTORY_SEPARATOR, $segments) . DIRECTORY_SEPARATOR;
			$filename = str_replace(':', '/', current($params));
			$extension = substr($filename, strrpos($filename,'.')+1);
			$subfold = in_array($extension, array('png','gif','jpg','svg'))  ? 'img' : $extension;
			$this->_sendResponse($filepath . $subfold . DIRECTORY_SEPARATOR . $filename);
		}
	}
}
