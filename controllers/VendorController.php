<?php

/**
 * DefaultController
 * 
 * @author
 * @version 
 */

require_once 'AssetsController.php';

class Maell_VendorController extends Maell_AssetsController {

	
	public function __call($methodName, $args)
	{
		
		$req = $_SERVER['REQUEST_URI'];
		$parts = explode('?', $req);
		
		$path = \Maell::$basePath . '/vendor' . str_replace('/maell/vendor','', $parts[0]);
			
		$extension = substr($path, strrpos($path,'.')+1);
		
			if (file_exists($path)) {
				$file = file_get_contents($path);
				$this->getResponse()->setHeader('Content-type', $this->_mimetypes[$extension])->setBody($file);
				$this->getResponse()->sendResponse();
				exit();

			} else {
				$this->getResponse()->setHttpResponseCode(404)->sendResponse();
				exit();
			}
	}
}
