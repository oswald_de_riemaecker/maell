<?php

namespace Maell\ObjectModel\Property;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel\Property\Exception;

/**
 * Class for a Date Property
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class DateProperty extends AbstractProperty {

	
	const TODAY			= 'TODAY';
	
	const TODAY_DATE	= 'STODAY';

	const TODAY_TIME	= 'TTODAY';
	
	const TOMORROW		= '+1';
	
	
	public function setValue($value)
	{
		switch ($value) {
			
			case self::TODAY:
				$value = date('Y-m-d H:i:s');
				break;
				
			case self::TOMORROW:
				$value = date('Y-m-d H:i:s', time()+86400);
				break;
			
			case self::TODAY_DATE:
				$value = date('Y-m-d');
				break;
				
			case self::TODAY_TIME:
				$value = date('H:i:s');
				break;
					
			default:
				if ($this->getParameter('format') && ! \Zend_Date::isDate($value, $this->getParameter('format'))) {
					throw new Exception(array("VALUE_NOT_A_DATE", array($this->_id, $value)));
				}
				break;
		}
		
		parent::setValue($value);
	}
	
	
	
	public function getDisplayValue($withTime = false)
	{
		return self::format($this->_value, $withTime);
	}
	
	
	/**
	 * Format a date or datetime string for display
	 * @param string $str
	 * @param boolean $time
	 * @return string
	 */
	static public function format($str, $time = false)
	{
		$parts = explode(' ', $str);
		if (count($parts) == 1) {
			$parts = explode('T', $str);
		}
			
		$date = explode('-', $parts[0]);
		$date = array_reverse($date);
		$date = implode('/', $date);
		
		if ($time && isset($parts[1])) {
			$hour = explode(':',$parts[1]);
			$date .= sprintf(' %sh%s', $hour[0], $hour[1]);
		}
		return $date;
	}
}
