<?php

namespace Maell\ObjectModel\Property;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

/**
 * Property class to use for string values
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class BlobProperty extends AbstractProperty {

	
	protected $_filename;
	
	
	/**
	 * Set a value for the property
	 *
	 * Value can be either:
	 * the full path to a file
	 * the binary content of the file
	 *
	 * @param string $value
	 */
	public function setValue($value)
	{
		// @todo implement constraints
		if (substr($value, 0, 1) == DIRECTORY_SEPARATOR) {
			return $this->setValueFromFile($value);
		} else {
			return parent::setValue($value);
		}
	}
	

/* 	public function getValue($param = null)
	{
	//	return $this->_value ? Backend::loadBlob($this->_parent, $this) : null;
	}
	 */

	public function setValueFromFile($file)
	{
		if (is_readable($file)) {
			$this->_value = file_get_contents($file);
			$this->_filename = $file;
		}
		return $this;
	}
}
