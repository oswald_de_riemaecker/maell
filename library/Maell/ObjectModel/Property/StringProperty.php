<?php

namespace Maell\ObjectModel\Property;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

/**
 * Property class to use for string values
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class StringProperty extends AbstractProperty {

	
	public function setValue($value)
	{
	    if (! is_string($value)) {
	        throw new Exception("Given value must be a string");
	    }
	    
	    if ($this->getParameter('constraints.minlength') && strlen($value) < $this->getParameter('constraints.minlength')) {
	        throw new Exception("Value length is lower than mandatory minimum of " . $this->getParameter('constraints.minlength'));
	    }
	    

	    if ($this->getParameter('constraints.maxlength') && strlen($value) > $this->getParameter('constraints.maxlength')) {
	        throw new Exception("Value length is greater than mandatory maximum of " . $this->getParameter('constraints.maxlength'));
	    }
	    
	    if ($this->getParameter('constraints.uppercase')) {
			$value = mb_strtoupper($value,'UTF-8');
		}
		
		if ($this->getParameter('constraints.lowercase')) {
			$value = mb_strtolower($value,'UTF-8');
		}
		
		return parent::setValue($value);
	}
}
