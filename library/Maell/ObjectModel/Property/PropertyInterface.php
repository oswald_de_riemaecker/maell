<?php

namespace Maell\ObjectModel\Property;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_ObjectModel
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

/**
 * Interface for Maell\ObjectModel\Property\* objects
 *
 * @category   maell
 * @package    Maell_ObjectModel
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
interface PropertyInterface {
	
	
	public function __construct($id, array $params = null);
	
	
	public function setValue($value);
	
	
	public function getValue($param);
	
	
	public function getLabel($lang = null);
	
	
	/**
	 * Return the displayable form of the current value
	 */
	public function getDisplayValue();
}