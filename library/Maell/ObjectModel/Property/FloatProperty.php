<?php

namespace Maell\ObjectModel\Property;
/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */


/**
 * Class for an Float Property
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class FloatProperty extends AbstractProperty {

	
	public function setValue($value)
	{
		if (! is_numeric($value)) {
			throw new Exception(array("VALUE_NOT_INTEGER_OR_FLOAT", $value));
		}
		parent::setValue((float) $value);
	}
	
	
	public function getDisplayValue()
	{
		return self::format($this->_value, $this->getParameter('constraints.precision'), $this->getParameter('constraints.percentage'));
	}
	
	
	static public function format($value, $precision = 2, $ispercentage = false)
	{
		if (! is_numeric($precision)) {
			$precision = 2;
		}
		setlocale(LC_NUMERIC, 'fr_FR.UTF-8');
		$lc = localeconv();
		$ret = number_format($value, $precision, $lc['decimal_point'], $lc['thousands_sep']);
		if ($ispercentage !== false) $ret .= '%';
		return $ret;
	}
}
