<?php

namespace Maell\ObjectModel\Property;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * @version    $Revision: 876 $
 */

use Maell\Core;

/**
 * Property class to use for enumeration of values
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class EnumProperty extends AbstractProperty {

	
	protected $_values = array();
	
	
	public function getValues()
	{
		$lang = \Maell::$lang;
		
		if (! isset($this->_values[$lang])) {
			
			$this->_values[$lang] = array();
			
			foreach ($this->getParameter('values') as $key => $value) {
			
				/* a generic label can be specified */ 
				if (isset($value['label'])) {
					$this->_values[$lang][$key] = $value['label'];
					
				} else if (isset($value[$lang])) {
					$this->_values[$lang][$key] = $value[$lang];
					
				} else {
					$this->_values[$lang][$key] = $value['en'];
				}
			}
		}
		
		return $this->_values[$lang];
	}
	
	
	public function getDisplayValue($key = null)
	{
		$values = $this->getValues();
		if (! $key) $key = $this->_value;
		return isset($values[$key]) ? $values[$key] : $key;
	}
}
