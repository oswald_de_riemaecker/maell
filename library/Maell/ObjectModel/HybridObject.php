<?php

namespace Maell\ObjectModel;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */


/**
 * Class providing basic functions needed to handle environment building.
 *
 * @category   maell
 * @package    Maell_ObjectModel
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class HybridObject extends BaseObject {

	
	public function __construct($val = null, array $params = null)
	{
		$this->_setParameterObjects();
	
		if (is_array($params)) {
			$this->_setParameters($params);
		}
		
		$this->_dataObject = new DataObject('Maell\ObjectModel\HybridObject');
	}
	
	
	public function mergeObject(BaseObject $source, array $properties = array())
	{
		foreach ($source->getDataObject()->getProperties() as $key => $property) {
			if (count($properties) > 0 && ! in_array($key, $properties)) {
				continue;
			} 
			$this->_dataObject->addProperty($property);
		}
	}
}
