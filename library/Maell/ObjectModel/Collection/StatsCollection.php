<?php

namespace Maell\ObjectModel\Collection;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel\Exception;
use Maell\ObjectModel\Collection;
use Maell\ObjectModel\Property\ObjectProperty;

/**
 * Class providing basic functions needed to handle environment building.
 *
 * @category   maell
 * @package    Maell_ObjectModel
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class StatsCollection extends Collection {
	
	
	protected $_statsProps = array();
	
	
	public function __construct($do = null, array $params = null)
	{
		parent::__construct('Maell\ObjectModel\Collection\StatsObject', $params);
	}
	
	
	public function setStatsProps($properties)
	{
		$this->_statsProps = array();
		foreach ($properties as $property) {
			$this->_statsProps[$property->getId()] = $property;
		}
		
		return $this;
	}
	
	
	public function toArray($propertyAsKey = null, $callback = null)
	{
		if (is_null($propertyAsKey)) {
			rewind($this->_statsProps);
			$propertyAsKey = key($this->_statsProps);
		}
		
		if (! array_key_exists($propertyAsKey, $this->_statsProps)) {
			throw new Exception("No such property %0 in stat collection", array($propertyAsKey));
		}
		
		$property = $this->_statsProps[$propertyAsKey];
		$key = $property->getId();
		
		$array = array();
		foreach ($this->getMembers() as $member) {
			$group = $member->getGroup();
			if ($property instanceof ObjectProperty) {
				if (! $group[$key]->getUri()) continue;
				$str = sprintf('%s (%d)', $group[$key]->__toString(), $member->getTotal());
				$array[$group[$key]->getIdentifier()] = $str;
			} else {
				$str = sprintf('%s (%d)'
								, $callback ? call_user_func($callback, $group[$key]) : $group[$key]
								, $member->getTotal()
						      );
				$array[$group[$key]] = $str;
			}
		}
		
		if ($property instanceof ObjectProperty) {
			asort($array, SORT_STRING);
		} else {
			ksort($array, SORT_STRING);
		}
		return $array;
	}
}
