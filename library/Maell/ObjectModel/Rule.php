<?php

namespace Maell\ObjectModel;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */


/**
 * Class for Property.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Rule {

	
	/**
	 * Returns a Maell\ObjectModel\Rule\*Rule instance build from parameters
	 *
	 * @param string $id
	 * @param string $type
	 * @param array  $params
	 * @return Maell\ObjectModel\Rule\RuleAbstract
	 * @throws Maell\ObjectModel\Rule\Exception
	 */
	static public function factory($type = 'string', array $params = null)
	{
		$className = sprintf('\Maell\ObjectModel\Rule\%sRule', ucfirst(strtolower($type)));
		
		try {
			$rule = new $className($params);
			if (! $rule instanceof Rule\RuleInterface) {
				throw new Rule\Exception("$className doesn't implement Maell\ObjectModel\Rule\RuleInterface");
			}
			return $rule;

		} catch (Exception $e) {
			throw new Exception("RULE_INSTANCIATION_ERROR", $e->getCode(), $e);
		}
	}
}
