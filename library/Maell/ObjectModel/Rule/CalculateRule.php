<?php

namespace Maell\ObjectModel\Rule;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel;
use Maell\ObjectModel\Property;
use Maell\ObjectModel\DataObject;

/**
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class CalculateRule extends RuleAbstract {
	
	
	public function setSource($str)
	{
		/*
		 * Supported formulas are pretty basic: each element must separated from other by one space 
		 */
		
		/* @todo write/use a more complex formula parser */
		$elem = explode(' ', $str);
		
		$this->_source = $elem;
		
		return $this;
	}
	
	/**
	 * Copy value or method return of source property to destination property
	 * 
	 *  @param Maell\ObjectModel\DataObject $do
	 *  @return boolean
	 */
	public function execute(DataObject $do)
	{
		try {
			$prep = array();
			
			foreach ($this->_source as $elem) {
				if (in_array($elem, array('+', '-', '/', '*'))) {
					$prep[] = $elem;
				} else {
					if (strpos($elem, '.') !== false) {
						$props = explode('.', $elem);
						$value = $do;
						foreach ($props as $prop) {
							if ($value instanceof \Maell\ObjectModel\DataObject) { 
								$value = $value->getProperty($prop)->getValue();
							} else if ($value instanceof ObjectModel\BaseObject) {
								$value = $value->getProperty($prop);
							} else if ($value instanceof Property\AbstractProperty) {
								$value = $value->getValue();
							}
						}
						
						$prep[] = ($value instanceof Property\AbstractProperty) ? $value->getValue() : $value;
					} else {
						$prep[] = $do->getProperty($elem)->getValue();
					}					
				}
			}

			$value = (float) eval(sprintf('return %s;', implode($prep)));

			/* set destination value with source value */
			if (is_array($this->_destination)) {
				$do->getProperty($this->_destination[0])->getProperty($this->_destination[1])->setValue($value);
			} else {
				$do->getProperty($this->_destination)->setValue($value);
			}
		} catch (Exception $e) {
			echo $e->getTraceAsString();
			return false;
		}
		
		return true;
	}
}
