<?php

namespace Maell\ObjectModel\Rule;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * @version    $Revision: 832 $
 */

use Maell\ObjectModel,
	Maell\ObjectModel\Property;

/**
 * Abstract abstract class
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
abstract class AbstractRule extends ObjectModel\ObjectModelAbstract implements RuleInterface {


	/**
	 * Reference to BaseObject-derived instance
	 * @var Maell\ObjectModel\BaseObject
	 */
	protected $_object;
	
	
	protected $_source;
	
	
	protected $_destination;

	
	public function __construct(array $params = null)
	{
		/* deal with class parameters first */
		$this->_setParameterObjects();
		
		if (is_array($params)) {
			
			$this->_setParameters($params);
		}
	}
	
	
	public function setObject(ObjectModel\BaseObject $object)
	{
		$this->_object = $object;
		return $this;
	}
	
	
	public function setSource($array)
	{
		$this->_source = $this->_setElement($array);
		return $this;
	}
	

	public function setDestination($array)
	{
		$this->_destination = $this->_setElement($array);
		return $this;
	}
	

	protected function _setElement(array $array)
	{
		$element = new RuleElement();
		
		if (isset($array['argument'])) {
			
			$element->setArgument($array['argument']);
		}
		
		if (isset($array['property'])) {
					
			$element->setType(RuleElement::TYPE_PROPERTY)->setValue($array['property']);
					
		} else if (isset($array['method'])) {
		
			$element->setType(RuleElement::TYPE_METHOD)->setValue($array['method']);
		}
		
		return $element;
	}
	
	
	public function execute(Property\AbstractProperty $property = null)
	{
		return true;
	}
}
