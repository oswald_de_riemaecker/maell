<?php

namespace Maell\ObjectModel\Rule;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */


use Maell\ObjectModel;
use Maell\ObjectModel\Property;

/**
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class SimpleRule extends AbstractRule {
	
	
	/**
	 * Execute the given method or function
	 * 
	 *  @param Maell\ObjectModel\Property\AbstractProperty $obj
	 *  @return boolean
	 */
	public function execute(Property\AbstractProperty $property = null)
	{
		$do = $this->_object->getDataObject();
		
		try {
			$source = $this->_source->getValue();
			
			/* get source value */
			if (! $this->_source->isMethod()) {
				throw new Exception("Source must be a method or a function");
			}

			// recursion
			if (strstr($source, '.') !== false) {
				$parts = explode('.', $source);
				foreach ($parts as $part) {
					$property = $do->getProperty($part);
					if ($property instanceof Property\AbstractProperty) {
						if ($property->getValue() instanceof ObjectModel\ObjectModelAbstract){
							$obj = $property->getValue();
						}
					} else {
						$source = $part;
					}
				}
			} else {
				$obj = $this->_object;
			}

			/**
			 * IMPORTANT: if $obj is a collection from an unsaved object
			 * 			  don't execute rule because there is no uri yet to bind members to object
			 * @todo improve this kind of detection (a switch on the collection or the property itself ?)
			 */
				
			if ($obj instanceof ObjectModel\Collection && $this->_object->getUri() == null) {
				return;
			}
				
			$value = $obj->$source($this->_source->getArgument());
			
		} catch (Exception $e) {
			
			/* @todo log exception */
			$this->_object->setStatus('Exception executing rule: ' . $e->getMessage(), 0, array('rule' => __CLASS____));
			return false;
		}
		return true;
	}
}
