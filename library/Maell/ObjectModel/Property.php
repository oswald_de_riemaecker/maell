<?php

namespace Maell\ObjectModel;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel\Property;
use Maell\Core\Tag;

/**
 * Class for Property.
 *
 * @category   maell
 * @package    Maell_Property
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Property {

	
	const UNDEFINED_LABEL	= "No defined display value";
	
	
	const EMPTY_VALUE		= '_NONE_';
	
	
	/* constraints that can be enforced on a property's value */
	
	/**
	 * To provide a value is mandatory
	 * use <mandatory/> in your XML
	 * @var string
	 */
	const CONSTRAINT_MANDATORY		= 'mandatory';
	
	/**
	 * Value is unique
	 * use <unique/> in your XML
	 * @var string
	 */
	const CONSTRAINT_UNIQUE			= 'unique';

	/**
	 * Value is protected, once setted and saved, it can't be changed
	 * use <protected/> in your XML
	 * @var string
	 */
	const CONSTRAINT_PROTECTED		= 'protected';

	
	/**
	 * Related collection persistence level
	 * @var string
	 */
	const CONSTRAINT_PERSISTENT		= 'persistent';
	
	
	/**
	 * Value must be encrypted before being stored (password)
	 * use <encrypted/> in your XML
	 * @var unknown_type
	 */
	const CONSTRAINT_ENCRYPTED		= 'encrypted';
	
	/**
	 * Value's minimum length
	 * use <minlength>NN</minlength> in your XML
	 * @var string
	 */
	const CONSTRAINT_MINLENGTH		= 'minlength';
	
	/**
	 * Value's maximum length
	 * use <maxlength>NN</maxlength> in your XML
	 * @var string
	 */	
	const CONSTRAINT_MAXLENGTH		= 'maxlength';
	
	/**
	 * Value must contain letter(s)
	 * use <hasletters>NN</hasletters> in your XML
	 * @var string
	 */
	const CONSTRAINT_HASLETTERS		= 'hasletters';

	/**
	 * Value must contain digit(s)
	 * use <hasdigits>NN</hasdigits> in your XML
	 * @var string
	 */
	const CONSTRAINT_HASDIGITS		= 'hasdigits';
	
	/**
	 * Value is an array containing key=>value pairs where
	 * key is a valid language code 
	 * @var string
	 */
	const CONSTRAINT_MULTILINGUAL   = 'multilingual';
	
	/**
	 * Value is a valid email address
	 * use <emailaddress/> in your XML
	 * @var string
	 */
	const CONSTRAINT_EMAILADDRESS	= 'emailaddress';
	
	/**
	 * Value is a valid URL scheme
	 * use <urlscheme/> in your XML
	 * @var string
	 */
	const CONSTRAINT_URLSCHEME		= 'urlscheme';
	
	
	const CONSTRAINT_UPPERCASE		= 'uppercase';
	
	
	const CONSTRAINT_LOWERCASE		= 'lowercase';

	const CONSTRAINT_DATEMIN		= 'datemin';
	
	const CONSTRAINT_DATEMAX		= 'datemax';
	
	const CONSTRAINT_HOURMIN		= 'hourmin';
	
	const CONSTRAINT_HOURMAX		= 'hourmax';
	
	const CONSTRAINT_MINUTERANGE	= 'minuterange';
		
	
	/**
	 * Value represents the maximum acceptable size
	 * for a media
	 * @var string
	 */
	const CONSTRAINT_MAXSIZE		= 'maxsize';
	
	
	/* available options to get a Maell\ObjectModel\Property\* stored value */
	
	const URI		= 'uri';
	
	const DATA		= 'object';
	
	const OBJECT	= 'data';
	
	
	/**
	 * Returns a Maell\ObjectModel\PropertyAbstract-derived instance build from parameters
	 *
	 * @param string $id
	 * @param string $type
	 * @param array  $params
	 * @return Maell\ObjectModel\Property\AbstractProperty
	 * @throws Maell\ObjectModel\Property\Exception
	 */
	static public function factory($id, $type = null, array $params = null)
	{
		if (is_null($type) || ! is_string($type)) $type = 'string';
		
		$className = sprintf('\Maell\ObjectModel\Property\%sProperty', ucfirst(strtolower($type)));
		
		try {
			/* @var $property \Maell\ObjectModel\Property\AbstractProperty */
			$property = new $className($id, $params);

			if (! $property instanceof Property\AbstractProperty) {
				throw new Exception("$className is not extending Maell\ObjectModel\Property\AbstractProperty");
			}
			return $property;
		} catch (\Exception $e) {
			throw new Property\Exception(array("INSTANCIATION_ERROR",array($type, $e->getMessage())));
		}
	}
	
	

	/**
	 * Parse given object properties to return a string version
	 * @param ObjectModelAbstract $object
	 * @param string $display
	 * @return mixed|string
	 */
	static public function parseDisplayProperty(ObjectModelAbstract $object, $display = null)
	{
		if (is_null($display) && $object instanceof BaseObject) {
			return $object->__toString();
		}
		
		if (substr($display,0,1) == '[') {
			
			// mask
			Tag\ObjectTag::$object = $object;
			return Tag::parse(substr($display, 1, strlen($display)-2));
			
		} else {
			
			$displayProps = explode(',', $display);
			if (count($displayProps) == 1 && $displayProps[0] == '') {
				return $object instanceof BaseObject ? $object->__toString() : null; //self::UNDEFINED_LABEL;
			
			} else {
				$displayValue = array();
				foreach ($displayProps as $disProp) {
					// display the identifier part of an uri
					if ($disProp == ObjectUri::IDENTIFIER) {
						$displayValue[] = $object->getUri() ? $object->getUri()->getIdentifier() : '';
					} else {
						// display property value, if property exists!
						if (($prop = $object->getProperty($disProp)) !== false) {
							$displayValue[] = $prop->getDisplayValue();
						}
					}
				}
			
				return implode(' ', $displayValue);
			}
		}
	}
}
