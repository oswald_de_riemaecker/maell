<?php

namespace Maell\Backend;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

/**
 * Class used to identify a backend
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * 
 */
class BackendUri {
	
	/**
	 * Alias name as defined
	 *
	 * @var string
	 */
	protected $_alias;
	
	/**
	 * Uri type (pseudo protocol)
	 *
	 * @var string
	 */
	protected $_type;
	
	/**
	 * Username
	 *
	 * @var string
	 */
	protected $_username;
	
	/**
	 * Password
	 *
	 * @var string
	 */
	protected $_password;
	
	/**
	 * Host part
	 *
	 * @var string
	 */
	protected $_host;
	
	/**
	 * Port part
	 *
	 * @var integer
	 */
	protected $_port;
	
	
	/**
	 * 
	 * Datastore name (may be DB or collection or any other meaningful value for backend)
	 * @var string
	 */
	protected $_dbname;
	
	/**
	 * Url part
	 * Defines access path to the ressource within the backend
	 *
	 * @var string
	 */
	protected $_url;
	
	
	/**
	 * Extra parameters needed by adapter
	 * @var unknown
	 */
	protected $_params = array();
	
	
	/**
	 * URI building from a string or an array
	 *
	 * @param string|array $params
	 */
	public function __construct($params = null)
	{
		if (is_array($params)) {
			if (!empty($params['alias'])) {
				$this->_alias = $params['alias'];
			}
			
			if (! empty($params['adapter'])) {
				$this->_type = $params['adapter'];
			}
				
			if (! empty($params['username'])) {
				$this->_username = $this->_getConfigValue($params['username']);		
			}
				
			if (! empty($params['password'])) {
				$this->_password = $this->_getConfigValue($params['password']);	
			}
				
			if (! empty($params['host'])) {
				$this->_host = $this->_getConfigValue($params['host']);
			}
				
			if (! empty($params['port'])) {
				$this->_port = (int) $this->_getConfigValue($params['port']);
			}

			if (! empty($params['params'])) {
				$this->_params = (array) $this->_getConfigValue($params['params']);
			}
			
			if (! empty($params['url'])) {
				$url = is_array($params['url']) ? $params['url'][\Maell::getEnvData('webEnv')] : $params['url'];
				$this->_url = $this->_getConfigValue($url);
				$elem = explode("/", $this->_url);
				$this->_dbname = $elem[0];
			}
				
		} else if (is_string($params)) {
			$regexp = "#(^(@[a-z0-9._-]+)|(([a-z0-9._-]+)://)?(((?<=://)[a-z0-9._-]+)?(?::)?((?<=:)[a-z0-9._-]+)?@)?([a-z0-9.-]+)?(?::)?((?<=:)[0-9]+)?)?(?:/)([a-z0-9._/-]+)?$#i";
			$array = null;
			preg_match($regexp, $params, $array);
			
			$this->_alias		= $array[2];
			$this->_type		= $array[4];
			$this->_username	= $array[6];
			$this->_password	= $array[7];
			$this->_host		= $array[8];
			$this->_port		= $array[9];
			$this->_url			= $array[10];
		}
	}
	
	
	
	/**
	 * Define backend alias
	 * 
	 * @param string $str
	 * @return Maell\Backend\BackendUri
	 */
	public function setAlias($str)
	{
		$this->_alias = $str;
		return $this;
	}
	

	/**
	 * Define backend type
	 * 
	 * @param string $type
	 * @return Maell\Backend\BackendUri
	 */
	public function setType($type)
	{
		$this->_type = $type;
		return $this;
	}
	
	
	public function getAlias()
	{
		return $this->_alias;
	}
	
	
	public function getType()
	{
		return $this->_type;
	}
	
	
	public function getUsername()
	{
		return $this->_username;
	}

	
	public function getPassword()
	{
		return $this->_password;
	}
	
	
	public function getHost()
	{
		return $this->_host;
	}
	
	public function getPort()
	{
		return $this->_port;
	}
	
	
	public function getUrl()
	{
		return $this->_url;
	}
	
	
	public function getDbName()
	{
		return $this->_dbname;
	}
	

	public function getParams()
	{
		return $this->_params;
	}
	
	
	public function overwriteUrl($url)
	{
		if (is_string($url)) {
			$this->_url = $url;
		}
	}
	

	/**
	 * Créer l'uri d'un backend à partir d'un objet Zend Config
	 *
	 * @param SimpleXMLElement $config
	 * @return \Maell\ObjectModel\ObjectUri
	 */
	static public function backendConfigToUri(\SimpleXMLElement $config)
	{
		$params = array();
		$attributes = (array) $config->attributes();
		
		if (isset($attributes['@attributes']['alias'])) {
			$params['alias'] = '@' . $attributes['@attributes']['alias'];
		}

		foreach ($config as $key => $val) {
			$params[$key] = (string) $val;
		}
		return new \Maell\ObjectModel\ObjectUri($params);
	}
	
	
	protected function _getConfigValue($var)
	{
		if (! is_array($var)) {
			return $var;
		} else {
			return (isset($var[\Maell::getEnvData('webEnv')])) ? $var[\Maell::getEnvData('webEnv')] : null; 
		}
	}
	
	
	public function __toString()
	{
		return \Maell\Backend::PREFIX. $this->_alias . '/';
	}
}
