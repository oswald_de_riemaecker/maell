<?php

namespace Maell\Backend\Adapter;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel\DataObject;

use Maell\Core,
	Maell\Backend,
	Maell\ObjectModel,
	Maell\ObjectModel\Collection;

/**
 * Class providing CSV methods to backend
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * 
 */
class CsvAdapter extends AbstractAdapter {


	protected $_basePath;	
	
		
	public function create(ObjectModel\DataObject $dataObj = null)
	{
		return file_put_contents($this->_basePath . $dataObj->getUri()->getUrl(), $dataObj);
	}
	
	
	public function read(ObjectModel\DataObject $do)
	{
		/* @todo read line with given index */
		return array() ; //file_get_contents($this->_basePath . $do->getUri()->getUrl());
	}
	
	
	public function update(ObjectModel\DataObject $do)
	{
		throw new Exception(__CLASS__ . " backend doesn't implement the update() method");
	}
	
	
	public function delete(ObjectModel\DataObject $do)
	{
		return unlink($this->_basePath . $do->getUri()->getUrl());
	}
	
	
	public function find(Collection $collection, $returnCount = false)
	{
		$url = str_replace('{basepath}', \Maell::$basePath, $this->_uri->getUrl());
		$class = $collection->getDataObject()->getClass();
		
		if ($this->_mapper) {
			
			$file = $this->_mapper->getDatastore($class);
				
		} else {
			
			throw new Exception("Csv backends need a mapper where datastores are defined");
		}


		if (substr($file, 0, 1) != DIRECTORY_SEPARATOR) $file = $url . $file;
		
		try { 
				$file = fopen($file, 'r');
				
		} catch (Exception $e) {
			
			throw new Exception("Error opening file: " . $e->getMessage());
		}

		// prepare defined conditions to be used next
		$this->_prepareConditionBlock($collection);
		
		// populate array with relevant objects type
		$uri = new ObjectModel\ObjectUri();
		$uri->setBackendUri($this->_uri);
		$uri->setClass($class);

		if ($collection->getParameter('memberType') != 'uri') {
			
			$do = new ObjectModel\DataObject($class);
		}

		$separator = $this->_mapper->getExtraArg('separator', $class);
		
		if (! $separator) $separator = ',';
		
		$separator = str_replace(array('{tab}'), array("\t"), $separator);
		
		$array = array();
		
		$key = -1; // csv file current line key
		$selected = 0; // selected members counter
		$offset = $collection->getBoundaryOffset();
		$limit = $collection->getBoundaryBatch();
		
		while (($data = fgetcsv($file, 1000, $separator)) !== false) {	
			
			if ($this->_mapper->getExtraArg('firstlineisheader', $class) !== false && $key == -1) {
				
				/* ignore first line if it is declared as header */
				$key += 1;
				$offset += 1;
				continue;
			}

			/* test data array against defined conditions */
			if ($this->_testAgainstConditionBlock($data) === false) {
			
				continue;
			}
			
			$key++;
			
			// ignore everything before offset value
			if ($key < $offset && $returnCount != true) continue;
			
			// break after limit value is reached except if we count lines
			if ($selected == $limit && $returnCount !== true) {
				return $array;
			}
			
			// increment selected counter
			$selected++;
			
			if ($returnCount !== true) {

				$uri = new ObjectModel\ObjectUri();
				$uri->setBackendUri($this->_uri);
				$uri->setClass($collection->getDataObject()->getClass());
				$uri->setUrl($url . '/' . $key);
						
				$do = DataObject::factory($class);
				$do->setUri($uri);
				$do->populate($data, $this->_mapper);
				$array[] = new $class($do);
			}
		}
		
		return $returnCount ? $key : $array;
	}
	
	
	/**
	 * Convert an array of Maell\Backend\Condition instances into an array of preg_match patterns
	 * AND is the only supported mode bewteen conditions right now
	 * AND & OR mode are supported within the clauses of the same condition
	 * 
	 * @todo implement AND mode between conditions
	 * @param Maell\ObjectModel\Collection $collection
	 */
	protected function _prepareConditionBlock(Collection $collection)
	{
		$array = array();
		
		foreach ($collection->getConditions() as $condition) {
			/* get property id in backend */
			$key = $this->_mapper->propertyToDatastoreName($collection->getClass(), $condition[0]->getProperty()->getId());
			$array[$key] = array();
			$clauses = $condition[0]->getClauses();
			$value = array();
			foreach ($clauses as $clause) {
				if (! isset($mode)) $mode = isset($clause['mode']) ? $clause['mode'] : Backend\Condition::MODE_AND;
				$ops = $this->_matchOperator($clause['operator']);
				$prefix = $suffix = null;
				foreach ($ops as $op) {
					switch ($op) {
				
						case Backend\Condition::OPERATOR_BEGINSWITH:
							$prefix = '^'; 
							break;
						
						case Backend\Condition::OPERATOR_ENDSWITH:
							$suffix = '$';
							break;
							
						default:
							break;
					}
				}
				
				if (is_array($clause['value'])) {
					$value[] = $prefix . '[' . implode(",", $clause['value']) . ']' . $suffix;
				} else {
					$value[] = $prefix . $clause['value'] . $suffix;
				}
			}
			$array[$key][$mode] = $value;
			$mode = null;
		}

		$this->_conditionsBlock = $array;
	}
	
	
	/**
	 * Test an array against various preg_match patterns contained in $this->_conditionBlock
	 * 
	 * @todo implement OR mode handling for conditions
	 * @param array $data
	 * @return boolean
	 */
	protected function _testAgainstConditionBlock(array $data)
	{
		foreach ($this->_conditionsBlock as $key => $modes) {
			
			// We don't test value if value is not setted (but we should)
			if (! isset($data[$key])) continue;
			
			foreach ($modes as $mode => $patterns) {

				/* condition status, needed for OR mode */
				$status = false;
				
				foreach ($patterns as $pattern) {
					
					$result = preg_match("/" . $pattern . "/i", $data[$key]);
					//printf('%d -> %s preg %s -> %s<br/>', $key, $data[$key], $pattern, $result);
					
					/* in AND mode, any no-match returns failure */
					if ($mode == Backend\Condition::MODE_AND && $result == 0) {
						return false;
					}
					
					/* in OR mode, one match is enough */
					if ($mode == Backend\Condition::MODE_OR && $result != 0) {

						$status = true;
					}
				}
				
				if ($status == false && $mode == Backend\Condition::MODE_OR) {
					
					return false;
				}
			}
		}

		return true;
	}
}
