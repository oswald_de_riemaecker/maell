<?php

namespace Maell\Backend\Adapter;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * @version    $Revision: 880 $
 */

use Maell\Backend;
use Maell\ObjectModel;

/**
 * Class providing Filesystem methods to backend
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * 
 */



abstract class FilesystemAdapter extends AbstractAdapter {


	protected $_basePath;	
	
	
	/**
	 * Initialiser un backend à partir d'une Uri
	 *
	 * @param \Maell\Backend\BackendUri $uri
	 * @param string $alias
	 */
	public function __construct(Backend\BackendUri $uri, $alias = null)
	{
		parent::__construct($uri, $alias);
	}
	
	
	public function create(ObjectModel\DataObject $dataObj = null)
	{
		return file_put_contents($this->_basePath . $dataObj->getUri()->getUrl(), $dataObj);
	}
	
	
	public function read(ObjectModel\DataObject $do)
	{
		return file_get_contents($this->_basePath . $do->getUri()->getUrl());
	}
	
	
	public function update(ObjectModel\DataObject $do)
	{
		throw new Exception(__CLASS__ . " backend doesn't implement the update() method");
	}
	
	
	public function delete(ObjectModel\DataObject $do)
	{
		return unlink($this->_basePath . $do->getUri()->getUrl());
	}
	
	
	public function find(ObjectModel\Collection $co)
	{
		
	}
}
