<?php

namespace Maell\Backend\Adapter;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2015 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell as Maell;
use Maell\Backend;
use Maell\ObjectModel;
use Maell\Backend\Condition;
use Maell\ObjectModel\Property\ObjectProperty;
use Maell\ObjectModel\ObjectUri;

/**
 * class providing all methods to use with MongoDB
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2015 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class MongoAdapter extends AbstractAdapter {


    const OP_AND = '$and';
    
    const OP_OR  = '$or';
    
    
    static public $stripAccents = true;

    static public $accentsMap = [
	                   'c' => 'çÇ',
	                   'd' => 'Ð',
	                   'm' => 'µ',
	                   'n' => 'Ññ',
	                   's' => 'šßŠ',
	                   'a' => 'àáâãäåæÀÁÂÃÄÅÆ',
	                   'e' => 'èéêëẽÈÉÊËẼ',
	                   'i' => 'ìíîïĩÌÍÎÏĨ',
	                   'o' => 'œòóôõöøðÒÓÔÕÖØŒ',
	                   'u' => 'ùúûüÙÚÛÜ',
	                   'y' => 'ýÿÝŸ¥',
	                   'z' => 'žŽ'
                	  ];
    
	/**
	 * DB-bound connection
	 *
	 * @var MongoDB
	 */
	protected $_db;
	
	protected $_operators = array(Backend\Condition::OPERATOR_GTHAN => '$gt'
								, Backend\Condition::OPERATOR_LTHAN => '$lt'
								, Backend\Condition::OPERATOR_EQUAL => '$in'
								, Backend\Condition::OPERATOR_DIFF => '$nin'
								, Backend\Condition::OPERATOR_NEAR => '$near'
								 );
								 	
	/**
	 * Database name
	 * @var string
	 */
	protected $_database;
	

	/**
	 * Collection name
	 * 
	 * database name as mongodb names it
	 * @var string
	 */
	protected $_collection;
	
	
	/**
	 * if not null, count() queries must be avoided
	 * @var integer
	 */
	protected $_fixedCount;
	
	
	
	/**
	 * Instanciate a MongoDB backend from a Maell\Backend\BackendUri object 
	 *
	 * @param Maell\Backend\BackendUri $uri
	 * @param string $alias
	 * @throws Maell\Backend\Exception
	 */
	public function __construct(Backend\BackendUri $uri, $alias = null)
	{
		if (! extension_loaded('mongo')) {
			throw new Exception(array("BACKEND_REQUIRED_EXT", 'mongo'));
		}

		parent::__construct($uri, $alias);
		
		$url = explode('/', $uri->getUrl());
		
		/* @todo do we need to force database declaration in backend ? */
		if (! empty($url[0])) {
			$this->_database = $url[0];
		} else {
			throw new Exception('BACKEND_MISSING_DBNAME_PARAM');
		}
		
		if (! empty($url[1])) {
			$this->_collection = $url[1];
		}
	}
	
	
	protected function _connect()
	{
		if (! $this->_ressource) {
			try {
				$cstring = sprintf('mongodb://%s:%d/%s', $this->_uri->getHost(), $this->_uri->getPort(), $this->_uri->getUrl());
				$this->_ressource = new \MongoClient($cstring, $this->_uri->getParams());
				$this->_db = $this->_ressource->selectDB($this->_uri->getUrl());
			} catch (\MongoException $e) {
				throw new Exception($e->getMessage());
			} catch (\InvalidArgumentException $e) {
				throw new Exception($e->getMessage());			
			}
		}
	}
	
	
	/**
	 * Save new set of data from a Maell\ObjectModel\DataObject instance using INSERT 
	 *
	 * @param Maell\ObjectModel\DataObject $do
	 * @return boolean
	 * @throws Maell\Backend\Exception
	 */
	public function create(ObjectModel\DataObject $do)
	{
		// set database to use
		$this->_selectDatabase($do->getUri());
		
		// get collection to use, from mapper if available, else from data object
		$collection = ($this->_mapper instanceof Backend\Mapper) ? $this->_mapper->getDatastore($do->getClass()) : $do->getClass();

		$collec = $this->_db->selectCollection($collection);
		
		// get a valid data array passing mapper if any
		if ($this->_mapper) {
			$recordSet = $do->map($this->_mapper, 'backend', $this->_uri->getAlias());
		} else {
			$recordSet = $do->toArray($this->_uri->getAlias());
		}
		
		try {
			$collec->insert($recordSet['data']);
		} catch (\Exception $e) {
			\Zend_Debug::dump($e);
			return false;
		}
		
		// inject new ObjectUri object in data object
		$uri = Backend::PREFIX . $this->_uri->getAlias() . '/' . $this->_database 
		     . '/' . $collection . '/' . $recordSet['data']['_id']->__toString();
		
		$uri = new ObjectModel\ObjectUri($uri, $this->_uri);
		$do->setUri($uri);
		
		return true;
	}
	
	
	
	/**
	 * Populate the given data object
	 *  
	 * @param Maell\ObjectModel\DataObject $do data object instance
	 * @return boolean
	 */
	public function read(ObjectModel\DataObject $do, array $data = null) 
	{
		$uri = $do->getUri();
		
		if (! is_array($data) || count($data) == 0) {
		
		  // set database to use
		  $this->_selectDatabase($uri->getBackendUri());
		
		  // get table to use, from mapper if available, else from data object
		  $collection = ($this->_mapper instanceof Backend\Mapper) ? $this->_mapper->getDatastore($uri->getClass()) : $uri->getClass();

		  $collec = $this->_db->selectCollection($collection);
		
		  // get data from backend
		  $data = array('_id' => new \MongoId($uri->getIdentifier()));
		  $this->_setLastQuery('findOne', $data, ['collection' => $collection]);
		  $data = $collec->findOne($data);
		
		  if (empty($data)) {
			 return null;
		  }
		
		  /* complete url part of the object uri */
		  $do->getUri()->setUrl($this->_database . '/' . $collection . '/' . $do->getUri()->getIdentifier());
		}
		
		// convert MongoDate instances into timestamp
		foreach ($data as $key => $val) {
			if ($val instanceof \MongoDate) {
				$data[$key] = date('Y-m-d H:i:s', $val->sec);
			} else if (is_array($val) && isset($val['$id'])) {
				// convert MongoRef-like array
				$data[$key] = $val['$id'];
			} else if(is_array($val)) {
				$data[$key] = $this->_convert2SimpleArray($val);
			}
		}
		/* populate data object */
		$do->populate($data, $this->_mapper);
	}
	
	
	/**
	 * Convert array of k:v objects into simpler array
	 * @param array $array
	 * @param array $keys
	 * @return array
	 */
	protected function _convert2SimpleArray(array $array, $keys = ['__'])
	{
		if (Maell::$lang) $keys[] = Maell::$lang;
		$narr = [];
		foreach ($array as $element) {
			if (! is_array($element) || ! isset($element['k'])) {
				continue;
			}
			$narr[$element['k']] = $element['v'];
		}
		return count($narr) > 0 ? $narr : $array;
	}
	
	
	protected function _convert2ComplexArray(array $array)
	{
	    $narr = [];
	    foreach ($array as $key => $val) {
	        if (! is_array($val)) {
	            continue;
	        }
	        foreach ($val as $k => $v) {
    	        if (is_numeric($k)) {
	               continue;
	            }
	           $narr[] = ['k' => $k, 'v' => $v];
	        }
	    }
	    return count($narr) > 0 ? $narr : $array;
	}
	
	
	/**
	 * Update the given data object
	 * @param ObjectModel\DataObject $do
	 * @throws Exception
	 * @return boolean
	 */
	public function update(ObjectModel\DataObject $do)
	{
		$uri = $do->getUri();
		
		$this->_selectDatabase($do->getUri());
		// get collection to use, from mapper if available, else from data object
		$collecName = ($this->_mapper instanceof Backend\Mapper) ? $this->_mapper->getDatastore($do->getClass()) : $do->getClass();
		$collec = $this->_db->selectCollection($collecName);
		
		// Mapping et conversion de valeurs
		$data = $this->_mapper ? $do->map($this->_mapper, $this) : $data = $do->toArray($this, true);
		
		foreach ($data['data'] as $key => $val) {
		    if (false) {
		        // convert MongoRef-like array
		        $data[$key] = ['$_id' => $val['$id'], '$ref' => '', 'code' => []];
		    } else if (is_array($val)) {
		        $data['data'][$key] = $this->_convert2ComplexArray($val);
		    }
		}
		return (bool) $collec->update(['_id' => new \MongoId($do->getUri()->getIdentifier())], [ '$set' => $data['data'] ]);
	}
	
	
	/**
	 * Delete record in backend 
	 * 
	 * @param Maell\ObjectModel\DataObject $do
	 * @return boolean
	 */
	public function delete(ObjectModel\DataObject $do)
	{		
		// on découpe l'url de l'URI afin d'obtenir les parties qui nous interesse.
		$uri = $do->getUri();
		
		// on trouve la base de donnée à utiliser
		$database = $this->_selectDatabase($uri);
		
		// on trouve la table à utiliser
		$table = $this->_selectTable($uri);
		
		//return $this->_ressource->delete($table, $table . "_id = " . $url['id']);
	}
	
	
	/**
	 * Returns an array of objects queried from the given Maell\ObjectModel\Collection instance parameters
	 *
	 * The given collection is populated if it comes empty of members.
	 *
	 * In any other case, this method doesn't directly populate the collection. This action is under the responsability of
	 * the caller. For example, the Collection::find() method takes care of it.
	 *
	 * @param Maell\ObjectModel\Collection $collection
	 * @param boolean|array $returnCount
	 * @return array
	 */
	public function find(ObjectModel\Collection $collection, $returnCount = false)
	{
	    $this->_fixedCount = null;
		$class = $collection->getDataObject()->getClass();
		$mode  = $collection->getParameter('memberType');
		
		// set database to use
		$this->_selectDatabase($collection->getDataObject()->getUri());
		
		// enable profiler
		$this->_db->setProfilingLevel(2);
		
		// get collection to use, from mapper if available, else from data object
		$collecName = ($this->_mapper instanceof Backend\Mapper) ? $this->_mapper->getDatastore($class) : $class;
		
		/* @var $collect \MongoCollection */
		$collec = $this->_db->selectCollection($collecName);

		$conditions = [self::OP_OR => [], self::OP_AND => []];
		
		foreach ($collection->getConditions() as $conditionArray) {
			// combo conditions
			if ($conditionArray[0] instanceof Condition\Combo) {
				foreach ($conditionArray[0]->getConditions() as $condition) { // $condition is array
					$condition = $condition[0];
					if ($this->_mapper) { // map property to field
						$field = $this->_mapper->propertyToDatastoreName($class, $condition->getProperty()->getId());
					} else {
						$field = $condition->getProperty()->getId();
					}
					if ($condition->getArrayKey()) {
						$field .= '.' . $condition->getArrayKey();
					}
					$conditions[self::OP_OR] = $this->_buildConditionStatement($condition, $class, $conditions[self::OP_OR]); 
				}
			} else {
    			$condition = $conditionArray[0];
    			$ckey = '$' . strtolower($conditionArray[1]);
    			if (! in_array($ckey, [self::OP_AND, self::OP_OR])) {
    			    throw new Exception("$ckey is not an acceptable conditions handler");
    			}
    			$conditions[$ckey] = $this->_buildConditionStatement($condition, $class, $conditions[$ckey]);
			}
		}
		
		/**
		 * $and and $or arrays
		 */
		foreach ($conditions as $key => $val) {
			if (count($val) == 0) {
				unset($conditions[$key]);
			}
		}

		if (count($conditions) > 0) {
    		$conditions = [ self::OP_AND => [$conditions] ];
		}
			
		//echo json_encode($conditions); die;
		
		if ($returnCount) {
		    if ($this->_fixedCount != null) {
		        return $this->_fixedCount;
		    }
			$this->_setLastQuery(sprintf('%s.%s.count(%s)', $this->_database, $collecName, json_encode($conditions)), [], ['collection' => $collecName]);
			return $collec->count($conditions);
		}

		
		$sortings = array();
		foreach ($collection->getSortings() as $sorting) {
			if ($this->_mapper) {
				$field = $this->_mapper->propertyToDatastoreName($class, $sorting[0]->getId());
			} else {
				$field = $sorting[0]->getId();
			}
			$sortings[$field] = $sorting[1] == 'ASC' ? \MongoCollection::ASCENDING : \MongoCollection::DESCENDING;
		}
		
		$ids = $collec->find($conditions) //, array_merge(array('_id'), $this->_getColumns($collection->getDataObject())))
 						->sort($sortings)
						  ->skip($collection->getBoundaryOffset())
						  	->limit($collection->getBoundaryBatch());
		$ids->timeout(-1);
		
		$this->_setLastQuery(sprintf('%s.%s.find(%s)%s'
				, $this->_database
				, $collecName
				, json_encode($conditions)
				, count($sortings) > 0 ? sprintf('.(%s)', json_encode($sortings)) : null
		),[], array('collection' => $collecName));
		
		/* prepare base of object uri */
		$uri = new ObjectModel\ObjectUri();
		$uri->setBackendUri($this->_uri);
		$uri->setClass($collection->getDataObject()->getClass());
		$uri->setUrl($this->_database . '/');
		return $this->_populateCollection(iterator_to_array($ids), $collection, $uri);
	}
	
	
	public function returnsDistinct(ObjectModel\Collection $collection, ObjectModel\Property\AbstractProperty $property)
	{
		$class = $collection->getDataObject()->getClass();
		$mode  = $collection->getParameter('memberType');
		
		// set database to use
		$this->_selectDatabase();
		
		// get collection to use, from mapper if available, else from data object
		$collec = ($this->_mapper instanceof Backend\Mapper) ? $this->_mapper->getDatastore($class) : $class;

		//$collec = $this->_db->selectCollection($collec);

		$conditions = array();
		
		foreach ($collection->getConditions() as $conditionArray) {
			
			$condition = $conditionArray[0];
			
			// map property to field
			if ($this->_mapper) {
				$field = $this->_mapper->propertyToDatastoreName($class, $condition->getProperty()->getId());
			} else {
				$field = $condition->getProperty()->getId();				
			}
			
			$conditions += $this->_buildConditionStatement($condition, $class, $conditions);
		}
		
		$params = array();
		$params['distinct'] = $collec;
		$params['key'] = $property->getId();
		$params['query'] = $conditions;
		
		$this->_setLastQuery('command', $params);
		$ids = $this->_db->command($params);

		/* @todo if property is an object, we should get all values from the list of ids */
		return isset($ids['values']) ? $ids['values'] : array();
	}
	
	
	/**
	 * Set database connection to use 
	 *
	 * @param Maell\Backend\BackendUri $uri
	 * @return string
	 * @throws Maell\Backend\Exception
	 */
	protected function _selectDatabase(Backend\BackendUri $uri = null)
	{
		$this->_connect();
		if (! empty($this->_database)) {
			$this->_db = $this->_ressource->selectDB($this->_database);
			return $this->_database;
		} else {
			throw new Exception("NO_SELECTED_DATABASE");
		}
	}
	
	
	/**
	 * Returns a condition statement string based on given field identifier and clause(s)
	 * 
	 * @param string	$field
	 * @param array		$clauses
	 * @param string 	$mode
	 * @return string
	 */
	protected function _buildConditionStatement(Condition $condition, $class, $conditions = [])
	{
		$property = $condition->getProperty();
		
		// map property to field
		if ($this->_mapper) {
			$field = $this->_mapper->propertyToDatastoreName($class, $property->getId());
		} else {
			$field = $property->getId();
		}
			
		if ($condition->getArrayKey()) {
			$field .= '.' . $condition->getArrayKey();
		}
		
		/* convert identifier tag to the valid primary key */
		if ($field == ObjectUri::IDENTIFIER) {
			$field = '_id';
		} else if ($property->getParameter('constraints.multilingual')) {
			// handle multilingual string fields
			$field .= '.v';
		}

		foreach ($condition->getClauses() as $key => $clause) {
			$ops = $this->_matchOperator($clause['operator']);
			$_operators = $this->_operators;
			$value = $clause['value'];
			$fuzzy = false;
			
			if ($field == '_id') {
			    $conditions[] = [$field => new \MongoId($value)];
			    return $conditions;
			}
			
			// specific case of geolocation condition
			if (array_sum($ops) == Condition::OPERATOR_NEAR) {
				/**
				 * Value is an array representing a Point and the maximum distance from it to allow :
				 * [0] => longitude
				 * [1] => latitude
				 * [2] => altitude
				 * [3] => max distance
				 */
				
				$conditions[] = [$field => 
									[$_operators[$ops[0]] => 
										[
										 '$geometry' => 
											[
												'type' => 'Point', 
												'coordinates' => [(float) $value[1], (float) $value[0]],
												'$maxDistance' => (float) $value[3]
											]
										]
									]
								];
				$this->_fixedCount = 30;
				return $conditions;
			}

			if ($property instanceof ObjectProperty && strlen($value) == 24) {
			    $field .= '.$id';
			    $ops = null;
			}
				
			if (is_array($value)) {
				$_operators[Backend\Condition::OPERATOR_EQUAL]	= '$in';
				$_operators[Backend\Condition::OPERATOR_DIFF]	= '$nin';
			} else {
			    if (self::$stripAccents === true) {
			        $value = self::stripAccents($value);
			    }
			    if (in_array(Backend\Condition::OPERATOR_BEGINSWITH, $ops) && in_array(Backend\Condition::OPERATOR_ENDSWITH, $ops)) {
			        $fuzzy = true;
			    }
				else if (in_array(Backend\Condition::OPERATOR_BEGINSWITH, $ops) && ! in_array(Backend\Condition::OPERATOR_ENDSWITH, $ops)) {
					$value = '^' . $value;
					$fuzzy = true;
				} else if (in_array(Backend\Condition::OPERATOR_ENDSWITH, $ops) && !in_array(Backend\Condition::OPERATOR_BEGINSWITH, $ops)) {
					$value .= '$';
					$fuzzy = true;
				}
			}
			
			if ($fuzzy) {
				$_operators[Backend\Condition::OPERATOR_EQUAL]	= '$in';
				$_operators[Backend\Condition::OPERATOR_DIFF]	= '$nin';
			}

			if (is_null($ops)) {
			    $conditions[] = [$field => $value];
			} else {
			    foreach ($ops as $op) {
			        if (isset($_operators[$op])) {
			            $cval = $fuzzy ? ['$regex' => $value, '$options' => 'i'] : $value;
			            $op = null;
			            if (! empty($_operators[$op])) {
			                $conditions[] = [ $field => [ $_operators[$op] => [$cval] ] ];
			            } else {
			                $conditions[] = [$field => $cval];
			            }
			        }
			    }
			}
		}
		return $conditions;
	}
	

	/**
	 * Populate the given collection from the array of identifiers and the uri base
	 *
	 * @param array $ids
	 * @param \Maell\ObjectModel\Collection $collection
	 * @param \Maell\ObjectModel\ObjectUri $uriBase
	 */
	protected function _populateCollection(array $ids, ObjectModel\Collection $collection, ObjectModel\ObjectUri $uriBase)
	{
	    $dataset = [];
		foreach ($ids as $key => $val) {
			$val[Backend::DEFAULT_PKEY] = $key;
			unset($val['_id']);
			$dataset[] = $val;
		}
		return parent::_populateCollection($dataset, $collection, $uriBase);
	}
	
	
	/**
	 * Change the given string to query in an 'accent insensitive' manner
	 * @param string $string
	 * @return string
	 */
	static public function stripAccents($string)
	{    
	    $string = str_split(utf8_decode($string));
	    $mapped = '';
	    foreach ($string as $char) {
	        $skip = false;
            foreach (self::$accentsMap as $key => $match) {
                if (strstr(utf8_decode($match), $char) !== false) {
                    $mapped .= '[' . $key . $match . ']';
                    $skip = true;
                    break;
                }
            }
            if (! $skip) $mapped .= $char;
	    }
	    return $mapped;
	}
}
