<?php

namespace Maell\Backend\Condition;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel\Property;
use Maell\ObjectModel\ObjectUri;
use Maell\ObjectModel\Collection;
use Maell\Backend\Condition;
use Maell\Backend\Exception;

/**
 * Simple class to handle query conditions on objects collections
 * 
 * each instance refers to a Maell\ObjectModel\Property\AbstractProperty passed as reference
 * and may contains several different clauses (value/operator pairs)
 * 
 * object instance may also contain another condition instance, especially
 * when the referenced property is handling an object reference. 
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Combo {
	

	protected $_collection;
	
	
	protected $do;
	
	
	/**
	 * Conditions array
	 * 
	 * @var array
	 */
	protected $_conditions;
	
	
	/**
	 * Class constructor
	 * 
	 * Property reference and value and operator of first clause may be defined here.
	 * It is also possible to define all elements at a later state with the relevant set() method.
	 * Clause is only defined upon the presence of a value.
	 *  
	 * @param Maell\ObjectModel\Collection
	 */
	public function __construct(Collection $collection)
	{
		$this->_collection = $collection;
		$this->_do = $this->_collection->getDataObject();
	}

	
	/**
	 * Returns the current nested Maell\Backend\Condition child instance
	 * or null
	 * 
	 * @return Maell\Backend\Condition
	 */
	public function getConditions()
	{
		return $this->_conditions;
	}
	
	
	public function setCondition(Property\AbstractProperty $property, $value = null, $operator = null, $mode = 'AND')
	{
		$condition = new Condition($property
								,  isset($value) ? $value : null
								, isset($operator) ? $operator : Condition::OPERATOR_EQUAL
									);
	
		$this->_conditions[] = array($condition, $mode);
	
		return $condition;
	}
	
	
	/**
	 * Set a new condition on property given id or throws an exception if property doesn't exist
	 *
	 * @param string $propertyName
	 * @param return Maell\Backend\Condition
	 * @throws Maell\Backend\Exception
	 */
	public function having($propertyName, $mode = Condition::MODE_AND)
	{
		/*
		 * If property name contains a '[', we are in the case of a condition on an
		* array member of the property (NoSQL-related)
		*/
		if (strstr($propertyName, '[') !== false) {
			list($propertyName, $arrayKey) = explode('[',$propertyName);
		}
		
		if ($propertyName == ObjectUri::IDENTIFIER) {
			return $this->setCondition(new Property\IdentifierProperty(ObjectUri::IDENTIFIER, null, null, $mode));
		} else if (($property = $this->_do->getProperty($propertyName)) !== false) {
			return $this->setCondition($property, null, null, $mode);
		} else if (strstr($propertyName, '.') !== false) {
			// deal with recursive properties
			$parts = explode('.', $propertyName);
			$condition = $this->setCondition($this->_do->getProperty($parts[0]));
	
			foreach (array_slice($parts,1) as $property) {
				$condition = $condition->having($property);
			}
			return $condition;
		}
		
		if (isset($condition)) {
			if (isset($arrayKey)) {
				$condition->setArrayKey($arrayKey);
			}
			return $condition;
		}
		
		throw new Exception(array("CONDITION_UNKNOWN_PROPERTY", $propertyName));
	}
}
