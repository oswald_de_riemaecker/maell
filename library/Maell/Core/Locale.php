<?php

namespace Maell\Core;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2015 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell;

/**
 * Class managing modules
 *
 * @category   maell
 * @package    Maell_Modules
 * @copyright  Copyright (c) 2015 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Locale {
    
    
    const DEFLANG = '__';
    
	
	static protected $_config;
	
	
	static public $lang = self::DEFLANG;
	

	static public function get(array $array)
	{
	    if (isset($array[self::$lang]) && ! empty($array[self::$lang])) {
	        return $array[self::$lang];
	    } else {
	        return $array[self::DEFLANG];
	    }
	}
}
