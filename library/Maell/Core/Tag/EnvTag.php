<?php

namespace Maell\Core\Tag;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

/**
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class EnvTag implements TagInterface {

	/**
	 * Return environment value described by tag and optional sub tag
	 * @param string $tag
	 * @param string $sub
	 * @return string
	 */
	static public function get($tag, $sub = null)
	{
		switch ($tag) {
				
			case 'date':
				return self::getDateVal($sub);
				break;
		}
	}
	
	
	/**
	 * Return a date calculated and formatted accordingly to parameters
	 * @param string $when
	 * @param string $pattern
	 * @return string
	 */
	static public function getDateVal($when = null, $pattern = 'Y-m-d H:i:s')
	{
		$ref = time();
	
		switch ($when) {
	
			case 'yesterday':
				$ref -= 86400;
				break;
	
			case 'tomorrow':
				$ref += 86400;
				break;
	
			case 'today':
			default:
				break;
		}
	
		return date($pattern, $ref);
	}
}
