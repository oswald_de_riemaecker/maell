<?php

namespace Maell\Core;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Backend
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\Core\Layout\Menu;

/**
 * Class managing menus
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Layout {


	static protected $_menus = array();
	
	static public $vendor;
	
	static public $moduleKey;
	
	static public $module;
	
	static public $controller;
	
	static public $action;
	
	
	public static function addMenu($id, Layout\Menu $menu)
	{
		self::$_menus[$id] = $menu;
	}
	
	
	public static function getMenu($id = null)
	{
		return is_null($id) ? self::$_menus : self::$_menus[$id];
	}
	
	
	public static function getCurrentMenu()
	{
		return self::getMenu(self::$module);
	}
	
	
	/**
	 * Return the current module array of parameters
	 * @return array
	 */
	public static function getCurrentModule()
	{
		return @Module::getConfig()[self::$vendor][self::$module];
	}
}
