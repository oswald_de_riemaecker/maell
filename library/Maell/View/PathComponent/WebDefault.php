<?php

namespace Maell\View\PathComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell;
use Maell\Core\Layout;
use Maell\View;
use Maell\View\Decorator\AbstractWebDecorator;

/**
 * Web Decorator for the MenuComponent View Element
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {
	
	
	public function render()
	{
		$elements = array();
		
		View::addCoreLib('style.css');
		$elements[] = sprintf('<a href="/">%s</a>', $this->_escape(Maell::$name));
		$module = Layout::getCurrentModule();
		$elements[] = $module['controller']['label'];
		if (false !== ($item = $this->_obj->getMenu()->getActiveItem())) {
			$elements[] = $item->getLabel();
		}
		
		if ($this->_obj->getSuffix()) {
			$elements[] = $this->_obj->getSuffix();
		}
		
		$separator = $this->_escape($this->_obj->getParameter('separator'));
		return '<div class="maell component path">' . "\n" . implode($separator, $elements) . "</div>\n";
	}
}
