<?php

namespace Maell\View\MenuComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View;
use Maell\View\Decorator\AbstractWebDecorator;
use Maell\Core\Layout;

/**
 * Web Decorator for the MenuComponent View Element
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebToggle extends WebDefault {
	
	
	protected $_currentItem;
	
	
	public function render()
	{
		View::addCoreLib('style.css');
		$html = '';

		if (false !== ($item = $this->_obj->getMenu()->getActiveItem())) {
			$this->_currentItem = $item->getId();
		}	
		foreach ($this->_obj->getMenu()->getItems() as $moduleKey => $module) {
			$menu = '';
			foreach ($module->getItems() as $item) {
				$menu .= $this->_renderMenu($item, $moduleKey);
			}
			
			if ($menu) {
				$label = $this->_escape($module->getLabel());
				if ($moduleKey == Layout::$module) {
					$label = sprintf('<strong>%s</strong>', $label);
				}
				// top level menu
				$html .= sprintf('<ul><li class="head" data-help="%s"><a class="head">%s</a><div class="drop">%s</div></li></ul>'
						, $this->_escape($module->getHelp())
						, $label
						, $menu
				);
			}
		}
	
		return $this->_getButton() . "\n" . '<div class="maell menu panel" id="menutoggle">' . "\n" . $html . "</div>\n";
	}

	protected function _getButton() {
		return '<button id="menutogglecontrol" onclick="jQuery(\'#menutoggle\').toggle(\'slide\')">Menu</button>';
	}
}
