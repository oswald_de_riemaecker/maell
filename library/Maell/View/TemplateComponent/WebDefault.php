<?php

namespace Maell\View\TemplateComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel\BaseObject;
use Maell\ObjectModel\MediaObject;
use Maell\ObjectModel\Property\AbstractProperty;
use Maell\View;
use Maell\View\ViewObject;
use Maell\View\Decorator;
use Maell\View\Decorator\AbstractWebDecorator;
use Maell\View\FormComponent\Element\MediaElement;
use Maell\ObjectModel\ObjectUri;


/**
 * Decorator class for template objects in a web context.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {
	
	
	const TAG_START = "%";
	
	const TAG_END	= "%";
	
	
	protected $_instanceof = 'Maell\View\TemplateComponent';
	
	
	/**
	 * Template content
	 * 
	 * @var string
	 */
	protected $_template;
		
	
	/**
	 * Parse a HTML template
	 * 
	 * @return string
	 */
    public function render()
	{
    	$this->_template = $this->_obj->getTemplate();
    	$tags = array();
    	 
    	$tagPattern = "/" . self::TAG_START . "([a-z0-9]+)\\:([a-z0-9_.]*)\\{*([a-zA-Z0-9:_.,\\\"']*)\\}*" . self::TAG_END . "/i";
    	preg_match_all($tagPattern, $this->_template, $tags, PREG_SET_ORDER);

    	// transform some characters (if template is not an html snippet)
    	if (strlen($this->_template) == strlen(strip_tags($this->_template))) {
	    	$this->_template = str_replace("\t", str_repeat("&nbsp;", 12), $this->_template);
    		$this->_template = str_replace("\n", "<br/>", $this->_template);
    	}	
		$this->_parseTags($tags);
    	
    	return $this->_template;
	}
	
	
	/**
	 * Parse given tags against current template
	 * 
	 * @param array $tags
	 */
	protected function _parseTags($tags)
	{
	    foreach ($tags as $tag) {
    		
	    	$value = null;
    		
    		switch($tag[1]) {
    			
    			case 'app':
    				$keys = explode('.', $tag[2]);
    				$value = sprintf('/maell/app/%s/%s/%s', $keys[0], $keys[1], $tag[3]);
    				break;
    				
    			case 'var':
	    			$keys = explode('.', $tag[2]);
    				$value = $this->_obj->getVariable($keys[0]);
    			
	    			if (count($keys) > 1) {
   						$value = $value[$keys[1]];
   					}
   					break;
    				
   				case 'env':
   					$value = View::getEnvData($tag[2]);
   					break;
   					
   				case 'container':
   					if (($templates = $this->_obj->getSubtemplates($tag[2])) !== false) {
   						$value = '';
   						foreach ($templates as $template) {
   							$deco = Decorator::factory($template);
   							$value .= $deco->render();
   						}
   					} 
   					break;
   					
   				default: // obj:
   					$tmp = explode('.', $tag[2]);
   					$obj = $this->_obj->getVariable($tmp[0]);
   					if ($obj instanceof MediaObject && isset($tmp[1])) {
   						// meta properties handling
   						switch ($tmp[1]) {
   							
   							case '_base64':
   								$value = sprintf('data:%s;base64,%s', $obj->getMime(), base64_encode($obj->loadBlob('media')));
   								break;
   								
   							case '_icon':
   								$value = 'file-' . $obj->getExtension();
   								break;
   								
   							case '_size':
   								$value = MediaElement::getDisplaySize($obj->getSize());
   								break;
   								
   							case '_url':
   								$value = MediaElement::getDownloadUrl($obj->getUri());
   							default:
   								break;
   						}
   					}
   					
   					if (! $value) {
	   					if ($obj instanceof BaseObject) {
		   					$value = isset($tmp[1]) && $tmp[1] == ObjectUri::IDENTIFIER ? $obj->getIdentifier() : $obj->getProperty($tmp[1]);
   							$value = ($value instanceof AbstractProperty)  ? $value->getDisplayValue() : $value;
   						} else {
   							$value = \Maell::$env == \Maell::ENV_DEV ? sprintf("Can't substitute any value to '%s'", $tag[0]) : null;
   						}
   					}
   					break;
   			}
   			
   			if ($value instanceof ViewObject) {
   				$deco = Decorator::factory($value);
   				$value = $deco->render();
   			} else {
   				//$value = $this->_escape($value);
   			}
    				
       		$this->_template = str_replace($tag[0], $value, $this->_template);
	    }
	}
}
