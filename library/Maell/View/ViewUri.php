<?php

namespace Maell\View;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * @version    $Revision: 832 $
 */

use Maell\View\ViewUri\Adapter\AbstractAdapter;

/**
 * Class providing basic URI manipulation methods.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class ViewUri {

	/**
	 * Instance of Uri builder class
	 *
	 * @var Maell\View\ViewUri\AbstractAdapter
	 */
	static protected $_uriAdapter;
	
	
	public static function setUriAdapter($adapter = 'get', array $params = null)
	{
		if ($adapter instanceof AbstractAdapter) {
			self::$_uriAdapter = $adapter;
			return self::$_uriAdapter;
		}
		
		$className = sprintf('\Maell\View\ViewUri\Adapter\%sAdapter', ucfirst(strtolower($adapter)));
		try {
			self::$_uriAdapter = new $className(null, $params);
		} catch (Exception $e) {
			throw new Exception("Unable to instanciate $className uri adapter: " . $e->getMessage());
		}
		return self::$_uriAdapter;
	}
	
	
	/**
	 * Return the current view uri adapter
	 * @return \Maell\View\Maell\View\ViewUri\AbstractAdapter
	 */
	public static function getUriAdapter()
	{
		if (self::$_uriAdapter instanceof AbstractAdapter) {
			return self::$_uriAdapter;
		} else {
			return self::setUriAdapter();
		}
	}
	
	
	/**
	 * Call the makeUri method of the instanciated adapter
	 * intented as a sortcut
	 *
	 * @param array $args
	 * @param boolean $noBase
	 * @return string
	 */
	public static function makeUri($args = null, $noBase = false)
	{
		self::getUriAdapter();
		return self::$_uriAdapter->makeUri($args, $noBase);
	}	
	
	
	public static function getIdentifier($identifier)
	{
		self::getUriAdapter();
		return self::$_uriAdapter->getIdentifier($identifier);
	}
	
	
	public static function getEnv()
	{
		self::getUriAdapter();
		return self::$_uriAdapter->getEnv();
	}
}
