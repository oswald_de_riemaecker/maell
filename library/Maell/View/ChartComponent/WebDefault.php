<?php

namespace Maell\View\ChartComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell as Maell;
use Maell\View;
use Maell\View\Decorator\AbstractWebDecorator;

/**
 * Web Decorator for the MapComponent View Element
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {
	
	
	public function render()
	{
		View::addVendorLib(sprintf('nnnick/chartjs/Chart%s.js', Maell::$env == Maell::ENV_PROD ? '.min' : ''));
		
		$this->_obj->setId('comp_' . substr(md5(microtime(true)), 0, 8));
		View::addEvent(sprintf('window.myPie = new Chart(document.getElementById("%s-chart-area").getContext("2d")).Pie(%s)'
				, $this->_obj->getId()
				, \Zend_Json::encode($this->_obj->getContent())
		), 'js');
	
		return sprintf('<div id="%s" class="maell component"><h4 class="title"><div class="icon"></div>%s</h4>
				<canvas id="%s-chart-area"/></div>'
				, $this->_obj->getId()
				, $this->_obj->getTitle()
				, $this->_obj->getId()
		);
	}
}
