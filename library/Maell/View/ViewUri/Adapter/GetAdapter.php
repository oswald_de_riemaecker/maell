<?php

namespace Maell\View\ViewUri\Adapter;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */


/**
 * Adapter for URI manipulation in GET context.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

class GetAdapter extends AbstractAdapter {

	/**
	 * Character used to glue key/pairs after collation
	 *
	 * @var string
	 */
	protected $_pairsSeparator	= '&';
	
	/**
	 * Character used to assign value to key in pairs
	 *
	 * @var string
	 */
	protected $_assignSeparator = '=';
	
	/**
	 * Character used to glue base uri and arguments string
	 *
	 * @var string
	 */
	protected $_partsSeparator	= '?';
		
	/**
	 * Array of sundries identifiers used to pass parameters 
	 *
	 * @var array
	 */
	protected $_identifiers = array('offset' => 'maello', 'search' => 'maellw', 'sort' => 'maells', 'query' => 'maellq');

	
	public function __construct($uriBase = null, $params = null)
	{
		$this->setEnv($_GET);
		parent::__construct($uriBase, $params);
	}
}
