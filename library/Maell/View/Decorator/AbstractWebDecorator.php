<?php

namespace Maell\View\Decorator;

use Maell\ObjectModel\Property;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use	Maell\View;
use Maell\View\Decorator\AbstractDecorator;

/**
 * Abstract class providing basic methods and parameters
 * for decorators in a Abstract context.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
abstract class AbstractWebDecorator extends AbstractDecorator {

	
	/**
	 * Constructors, calls parent and add CSS librairies
	 * @param Maell\View\ViewObject $obj
	 * @param array $params
	 */
	public function __construct($obj, array $params = null)
	{
		parent::__construct($obj, $params);
	}
	
	
	public function render()
	{
		View::addCoreLib('style.css');
		return $this->_headerRendering() . $this->_contentRendering() . $this->_footerRendering();
	}

	
	protected function _headerRendering()
	{
		return '';
	}
	
	
	protected function _footerRendering()
	{
		return '';
	}

	
	/**
	 * return an html-escaped version of the given string
	 * 
	 * @todo handle charset as an option
	 * @param string $str
	 * @param string charset
	 * @return string
	 */
	protected function _escape($str, $charset = 'utf-8')
	{
		return htmlentities($str, ENT_QUOTES, $charset);
	}
	
	
	protected function _nametoDomId($str)
	{
		return str_replace(array('[',']'), '_', $str);
	}
	
	/**
	 * Bind action to element in view
	 * 
	 * @param array $array
	 * @param string $callback
	 */
	protected function _bindAction(array $array, $callback = null)
	{
		$action = $array['obj'];
		$parameters = $array['parameters'];
		
		// prepare extra data (provided callback would replace default callback)
		$data = array('element' => $this->getId());
		if ($callback) $data['callback'] = $callback;
		
		// reduce action
		$reduced = $action->reduce(array('extra' => $data));
		
		if (isset($parameters['action'])) {
			$reduced['action'] = 'action/' . $parameters['action'];
		}
		View::addEvent(sprintf('maell.view.bind(%s)', \Zend_Json::encode($reduced)), 'js');
	}
	
	
	/**
	 * Add a Javascript observer and a callback function to the element if the given constraint is setted
	 * @param string $constraints
	 * @param string $suffix
	 */
	protected function addConstraintObserver($constraints, $suffix = null)
	{
		$constraints = (array) $constraints;
		foreach ($constraints as $constraint) {
			if (! $this->_obj->getConstraint($constraint)) {
				continue;
			}
		
			switch ($constraint) {
				case Property::CONSTRAINT_UPPERCASE:
					View::addEvent(sprintf("jQuery('form').delegate('[name*=\"%s\"]', 'blur', function() { jQuery(this).val(jQuery(this).val().toUpperCase())})"
											, $this->getId()
										  ), 'js');
					break;
				
				case Property::CONSTRAINT_LOWERCASE:
					View::addEvent(sprintf("jQuery('form').delegate('[name*=\"%s\"]', 'blur', function() { jQuery(this).val(jQuery(this).val().toLowerCase())})"
											, $this->getId()
										  ), 'js');
					break;
					
				case Property::CONSTRAINT_URLSCHEME:
					break;
				
				case Property::CONSTRAINT_EMAILADDRESS:
					break;
			}
		}
	}
}
