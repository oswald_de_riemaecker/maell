<?php

namespace Maell\View\FormComponent\Element;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */


/**
 * Maell Data Object handling a set of properties tied to an object
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class MultipleElement extends AbstractElement {
	

	public function setValue($val)
	{
		$this->_value = explode('|',$val);
		return $this;
	}
	
	
	/**
	 * (non-PHPdoc)
	 * @see \Maell\View\FormComponent\Element\AbstractElement::formatValue()
	 */
	public function formatValue($val = null)
	{
		$formatted = '';
		
		foreach ($val as $v) {
			if (isset($this->_enumValues[$v])) {
				if ($formatted) $formatted .= ', ';
				$formatted .= is_array($this->_enumValues[$v]) ? $this->_enumValues[$v][\Maell::$lang] : $this->_enumValues[$v];
			}
		}
		return $formatted;
	}
}
