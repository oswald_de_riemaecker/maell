<?php

namespace Maell\View\FormComponent\Element\TextElement;

use Maell\View\Decorator\AbstractWebDecorator;
use Maell\View;


class WebEditor extends AbstractWebDecorator {


	public function render()
	{
		$name =  $this->_obj->getAltId();
		
		$extraArgs = '';
		if ($this->getParameter('args')) {

			foreach ($this->getParameter('args') as $argKey => $argVal) {
				$extraArgs .= sprintf(' %s="%s"', $argKey, $argVal);
			}
		}
		$max = $this->_obj->getValueConstraint('maxval');
		$html  = sprintf('<textarea class="ckeditor" name="%s" cols="50" id="%s" rows="%s">%s</textarea>'
							, $name
							, $name
							, ($max > 10 || $max == 0) ? 10 : $max
							, $this->_obj->getValue()
						);
		
		View::addVendorLib('ckeditor/ckeditor/ckeditor.js');
		return $html;
	}
}
