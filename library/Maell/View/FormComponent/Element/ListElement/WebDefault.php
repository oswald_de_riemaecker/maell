<?php

namespace Maell\View\FormComponent\Element\ListElement;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel;
use Maell\View;
use Maell\View\ViewUri;
use Maell\View\Decorator\AbstractWebDecorator;
use Maell\ObjectModel\Property;

/**
 * Default web decorator for list elements
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	public function render()
	{
		if (($value = $this->_obj->getValue()) !== false) {
			if (($value instanceof ObjectModel\BaseObject || $value instanceof ObjectModel\DataObject) && $value->getUri()) {
				$value = $this->_obj->getParameter('altkey') ? $value->getProperty($this->_obj->getParameter('altkey'))->getValue() : $value->getUri()->getIdentifier();
			} else if ($value instanceof ObjectModel\ObjectUri) {
				$value = $value->getIdentifier();
			} else if ($this->_obj->getDefaultValue()) {
				$value = is_object($this->_obj->getDefaultValue()) ? $this->_obj->getDefaultValue()->getIdentifier() : $this->_obj->getDefaultValue();
			} else {
				$value = null;
			}
		}
		
		
		// display autocompleter field
		if ($this->_obj->getTotalValues() > $this->_obj->getParameter('selectmax')) {
			$params = array();
			foreach ($this->_params as $key => $param) {
				$params[$key] = $param->getValue();
			}
			$deco = new WebAutocomplete($this->_obj, $params);
			return $deco->render();
		} else {
			
			// set correct name for field name value depending on 'mode' parameter value
			$name = $this->_obj->getId();
			
			if ($this->getParameter('mode') == View\FormComponent::SEARCH_MODE) {
				$name = ViewUri::getUriAdapter()->getIdentifier('search') . '[' . $name . ']';
			}
			
			// display menu list
			$zv = new \Zend_View();
			$options = array(Property::EMPTY_VALUE => $this->getParameter('defaultlabel')) + (array) $this->_obj->getEnumValues();
			return $zv->formSelect($name, $value, null, $options);
		}
	}
}
