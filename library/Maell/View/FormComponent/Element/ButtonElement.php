<?php

namespace Maell\View\FormComponent\Element;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

/**
 * Maell Data Object handling a set of properties tied to an object
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class ButtonElement extends AbstractElement {

	
	/**
	 * 
	 * Maell\View\Action object associated with button
	 * @var \Maell\View\Action\AbstractAction
	 */
	protected $_action;
	

	/**
	 * Temp property while we decide wether a simple link on a button should be in an action class
	 * @var string
	 */
	protected $_link;
	
	
	/**
	 * Embedded image in button
	 * @var Maell\View\ImageComponent
	 */
	protected $_image;

	
	public function setLink($str)
	{
		$this->_link = $str;
		return $this;
	}
	
	
	public function getLink()
	{
		return $this->_link;
	}
	
	
	public static function factory($label, $link, array $params = array('icon' => 'tool-blue'))
	{
		$button = new self();
		$button->setTitle($label);
		$button->setLink($link);
		$button->setDecoratorParams($params);
		return $button;
	}
}
