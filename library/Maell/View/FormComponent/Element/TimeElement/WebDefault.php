<?php

namespace Maell\View\FormComponent\Element\TimeElement;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View\Decorator\AbstractWebDecorator;
use Maell\View;
use Maell\View\ViewUri;
use Maell\ObjectModel\Property\TimeProperty;

/**
 * Web decorator for the FieldElement class
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	public function render()
	{
		/* bind optional action to field */
		if ($this->_obj->getAction()) {

			$this->_bindAction($this->_obj->getAction());
			View::addEvent(sprintf("jQuery('#%s').focus()", $this->_id), 'js');
		}
		
		$name = $this->getId();
		
		if ($this->getParameter('mode') == View\FormComponent::SEARCH_MODE) {
			$name = ViewUri::getUriAdapter()->getIdentifier('search') . '[' . $this->_nametoDomId($name) . ']';
		}


		$html  = sprintf('<input type="hidden" name="%s" id="%s" value="%s"/>'
				, $name
				, $this->_obj->getId()
				, $this->_obj->getValue()
		);
		
		$zv = new \Zend_View();
		$options = array(null => $this->getParameter('defaultlabel')) + $this->_obj->getEnumValues(TimeProperty::HOUR_PART);
		$html .= $zv->formSelect($name . '_hour', $this->_obj->getValue(TimeProperty::HOUR_PART), null, $options);
		$options = array(null => $this->getParameter('defaultlabel')) + $this->_obj->getEnumValues(TimeProperty::MIN_PART);
		$html .= ' : ' . $zv->formSelect($name . '_minute', $this->_obj->getValue(TimeProperty::MIN_PART), null, $options);		

		View::addCoreLib('view:form.js');
		View::addEvent(sprintf("new maell.view.form.timeElement('%s')", $this->_obj->getId()), 'js');
		
		return $html;
	}
}
