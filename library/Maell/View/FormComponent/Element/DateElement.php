<?php

namespace Maell\View\FormComponent\Element;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */


/**
 * Maell Data Object handling a set of properties tied to an object
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class DateElement extends AbstractElement {

	
	const TODAY = '___NOW___';
	

	public function setDefaultValue($val)
	{
		 $this->_defaultVal = ($val == self::TODAY) ? date('Y-m-d') : $val;
	}
	
	
	public function formatValue($str = null, $fancy = false)
	{
		 if (! is_null($str)) {
		 	if (is_object($str)) {
		 		$str = $str->__toString();
		 	}
		 	
		 	$date = new \Zend_Date($str);
		 	return $fancy ? $date->toString(\Zend_Date::DATE_LONG) : $date->toString('dd/MM/yyyy');
			
		} else {
			
			return $str;
		}
	}
}
