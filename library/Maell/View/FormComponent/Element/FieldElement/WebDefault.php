<?php

namespace Maell\View\FormComponent\Element\FieldElement;

use Maell\ObjectModel\Property;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * @version    $Revision: 876 $
 */

use Maell\View\Decorator\AbstractWebDecorator,
	Maell\View,
	Maell\View\ViewUri;

/**
 * Web decorator for the FieldElement class
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends AbstractWebDecorator {

	
	public function render()
	{
		/* bind optional action to field */
		if ($this->_obj->getAction()) {
			$this->_bindAction($this->_obj->getAction());
			View::addEvent(sprintf("jQuery('#%s').focus()", $this->_id), 'js');
		}
		
		$name =  $this->getId();
		
		if ($this->getParameter('mode') == View\FormComponent::SEARCH_MODE) {
			$name = ViewUri::getUriAdapter()->getIdentifier('search') . '[' . $this->_nametoDomId($name) . ']';
		}

		$extraArgs = '';
		if ($this->getParameter('args')) {
			foreach ($this->getParameter('args') as $argKey => $argVal) {
				$extraArgs .= sprintf(' %s="%s"', $argKey, $argVal);
			}
		}

		$size = $this->getParameter('length');
		$max = $this->_obj->getConstraint(Property::CONSTRAINT_MAXLENGTH);
		$html  = sprintf('<input type="text" name="%s" id="%s" placeholder="%s" size="%s"%s value="%s"%s/>'
							, $name
							, $this->_obj->getId()
							, $this->_obj->getHelp()
							, ($max > $size || $max == 0) ? $size : $max
							, $max ? ' maxlength="' . $max . '"' : null
							, $this->_obj->getValue()
							, $extraArgs
						);
		
		$this->addConstraintObserver([
		                                  Property::CONSTRAINT_UPPERCASE,
		                                  Property::CONSTRAINT_LOWERCASE
                            		]);
		return $html;
	}
}
