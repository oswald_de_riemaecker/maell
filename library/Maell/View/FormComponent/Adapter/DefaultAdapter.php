<?php

namespace Maell\View\FormComponent\Adapter;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel;
use	Maell\ObjectModel\Property;
use	Maell\View\FormComponent\Element;
use	Maell\View\Exception;
use Maell\ObjectModel\Property\AbstractProperty;
use Maell\ObjectModel\Property\MetaProperty;

/**
 * Maell Data Object handling a set of properties tied to an object
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class DefaultAdapter extends AbstractAdapter {

	
	static public $constraintsList = array(Property::CONSTRAINT_MANDATORY
										 , Property::CONSTRAINT_UNIQUE
										 , Property::CONSTRAINT_PROTECTED
										 , Property::CONSTRAINT_ENCRYPTED
										 , Property::CONSTRAINT_EMAILADDRESS
										 , Property::CONSTRAINT_URLSCHEME
										 , Property::CONSTRAINT_MINLENGTH
										 , Property::CONSTRAINT_MAXLENGTH
	                                     , Property::CONSTRAINT_MULTILINGUAL
										 , Property::CONSTRAINT_HASDIGITS
										 , Property::CONSTRAINT_HASLETTERS
										 , Property::CONSTRAINT_UPPERCASE
										 , Property::CONSTRAINT_LOWERCASE
										 , Property::CONSTRAINT_DATEMIN
										 , Property::CONSTRAINT_DATEMAX
										 , Property::CONSTRAINT_HOURMIN
										 , Property::CONSTRAINT_HOURMAX
										 , Property::CONSTRAINT_MINUTERANGE
										 , Property::CONSTRAINT_MAXSIZE
										);

	
	public function addElementFromProperty(AbstractProperty $property, $fname, $position = null)
	{
		$class = get_class($property);
		$class = substr($class, strrpos($class, '\\')+1);
		
		switch ($class) {
			
			case 'EnumProperty':
				if ($property->getParameter('constraints.multiple') !== false) {
					$element = new Element\MultipleElement();
				} else {
					$element = new Element\EnumElement();
				}
				$element->setEnumValues($property->getValues());
				break;
				
			case 'DateProperty':
				$element = new Element\DateElement();
				break;

			case 'ArrayProperty':
				$element = new Element\ArrayElement();
				if (false !== ($keys = $property->getParameter('constraints.keys'))) {
				    // inject list of allowed keys if exists
				    $element->setConstraint('keys', (array) explode(',', $keys));
				}
				break;
				    
			case 'TimeProperty':
				$element = new Element\TimeElement();
				break;
									
			case 'CurrencyProperty':
				$element = new Element\CurrencyElement();
				break;

			case 'StringProperty':
				if ($property->getParameter('multilines')) {
					$element = new Element\TextElement();
				} else {
					$element = new Element\FieldElement();
				}
				break;
					
			case 'ObjectProperty':
				// @todo join this code and the one in CollectionProperty::getValue
				if ($property->getParameter('instanceof') == 'Maell\ObjectModel\MediaObject') {
					$element = new Element\MediaElement();
				} else {
					$element = new Element\ListElement();
					$element->setParameter('display', $property->getParameter('display'));
				
					$collection = new ObjectModel\Collection($property->getParameter('instanceof'));
				
					/* inject the condition that allows to find collection members */
					if ($property->getParameter('keyprop')) {
						$collection->having($property->getParameter('keyprop'))->equals($property->getParent());
					}
				
					if ($property->getParameter('depends')) {
						$element->setParameter('dependency',$property->getParameter('depends'));
					}
				
					if ($property->getParameter('morekeyprop')) {
						foreach ($property->getParameter('morekeyprop') as $value) {
							if (strstr($value, ' ') !== false) {
				
								// if value contains spaces, it's a pattern
								$parts = explode(' ', $value);
								if (count($parts) == 3) {
									if (strstr($parts[2],',') !== false) $parts[2] = explode(',', $parts[2]);
									$collection->having($parts[0])->$parts[1]($parts[2]);
								} else {
									if (strstr($parts[1],',') !== false) $parts[1] = explode(',', $parts[1]);
									$collection->having($parts[0])->equals($parts[1]);
								}
							} else {
								// default case, we expect the member to hold a property
								// with the same name and value as the current object
								$collection->having($value)->equals($property->getParent()->getProperty($value)->getValue());
							}
						}
					}
				
					if ($property->getParameter('sorting')) {
						$element->setParameter('sorting', $property->getParameter('sorting'));
					}
				
					if ($property->getParameter('search')) {
						$element->setParameter('search', $property->getParameter('search'));
					}

					if ($property->getParameter('sdisplay')) {
						$element->setParameter('sdisplay', $property->getParameter('sdisplay'));
					}
					
					$element->setCollection($collection);
				}
				break;
			
			case 'CollectionProperty':
				$element = new Element\GridElement();
				$element->setCollection($property->getValue());
				$element->setParameter('display', $property->getParameter('display'));
				break;
				
			case 'MediaProperty':
				$element = new Element\MediaElement();
				break;
					
				
			default:
				$element = new Element\FieldElement();
				break;

		}
		
		$element->setId(str_replace('.','-', $fname));
		$element->setTitle($property->getLabel());
		$element->setDefaultValue($property->getDefaultValue());
		$element->setHelp($property->getParameter('help'));
		
		if ($property instanceof MetaProperty) {
			$value = $property->getDisplayValue();
		} else {
			$value = $property->getValue();
			if ($value instanceof ObjectModel\ObjectUri) {
				$value = $value->__toString();
			} else if ($value instanceof ObjectModel\BaseObject) {
				$value = $value->getUri() ? $value->getUri()->__toString() : null;
			}
		}
		$element->setValue($value);
		
		$constraints = $property->getParameter('constraints');
		foreach (self::$constraintsList as $key) {
			if (isset($constraints[$key])) {
				$element->setConstraint($key, $constraints[$key] != '0' && empty($constraints[$key]) ? true : $constraints[$key]);
			}
		}
		
		// property uses a special format for which we should have a decorator
		if (isset($constraints['format'])) {
			$element->setDecorator($constraints['format']);
		}
		
		return $this->addElement($element, $position);
	}
	
	
	public function addElement($element, $position = null)
	{
		if (! $element instanceof Element\AbstractElement) {
			throw new Exception(array("OBJECT_NOT_INSTANCE_OF", 'Maell\View\FormComponent\Element\AbstractElement'));
		}
		return parent::addElement($element, $position);
	}
	
	
	public function validate()
	{
		$this->_errors = array();
		foreach ($this->_elements as $element) {
			$value = $this->_data[$element->getId()];
			/* test each form element for data consistency */
			if ($element) {
			}
		}
	}
}
