<?php

namespace Maell\View\FormComponent;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View;
use Maell\ObjectModel\BaseObject;
use Maell\ObjectModel\Property;
use Maell\View\FormComponent\Element;
use Maell\View\SimpleComponent;

/**
 * Web decorator for the FieldElement class
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class WebDefault extends SimpleComponent\WebDefault {

	
	protected $_instanceof = '\Maell\View\FormComponent';
	
	
	/**
	 * Maell\View\FormComponent instance
	 *
	 * @var Maell\View\FormComponent
	 */
	protected $_obj;


	/**
	 * Render form HTML and trigger maell.view.form() JS object
	 * @see \Maell\View\SimpleComponent\WebDefault::render()
	 */
    public function render()
    {   	
    	// cache object and create its client-side js counterpart
    	$reduced = $this->_obj->getSource()->reduce();
    	$this->_id = $this->_obj->getId() ? $this->_obj->getId() : 'maell_' . md5(time());
    	
    	View::addCoreLib(array('style.css','buttons.css','sprites.css'));
    	View::addCoreLib(array('core.js','view.js','view:form.js','view:alert.js'));
		View::addEvent(sprintf("maell.view.register('%s', new maell.view.form('%s',%s,%s))"
									, $this->_id
									, $this->_id
									, \Zend_Json::encode($reduced)
									, \Zend_Json::encode($this->_obj->reduce())
								  ), 'js');
    	
        return $this->_headerRendering() . $this->_contentRendering() . $this->_footerRendering();
    }
    
    
    protected function _contentRendering()
    {
		$p = '<fieldset id="form_fields">';
		//$p .= '<legend></legend>';
		
		$altDecorators = (array) $this->_obj->getParameter('decorators');
		
        foreach ($this->_obj->getColumns() as $key => $element) {
        	$field = $element;
        	/* hidden fields treatment */
        	if ($element->getConstraint(Element\AbstractElement::CONSTRAINT_HIDDEN) === true) {
        		$p .= sprintf('<input type="hidden" name="%s" id="%s" value="%s" />'
        					, $field->getAltId()
        					, $field->getAltId()
        					, $field->getValue()
        					);
        					
        		continue;
        	}
        	
        	if (! isset($focus) && $element instanceof Element\FieldElement) {
	        	View::addEvent(sprintf("jQuery('#%s').focus()", $element->getAltId()), 'js');
	        	$focus = $element;
        	}
        	
        	$label = '&nbsp;';
	        $label = $this->_escape($element->getTitle());
    	    if ($element->getConstraint(Property::CONSTRAINT_MANDATORY) == true) {
    	    	$class=' mandatory';
    	        $mandatory = ' mandatory';
    	    } else {
    	    	$class ='';
    	        $mandatory = '';
    	    }

    	    $line = sprintf('<div class="clear"></div><div id="label_%s" class="label%s"><label for="%s" data-help="%s">%s</label></div>'
            				, $field->getAltId()
    	    				, $class
    	    				, $field->getAltId()
    	    				, $field->getHelp()
            				, $label
            			 );
            
    	    // value is already defined and can't be changed
    	    if ($field->getConstraint(Property::CONSTRAINT_PROTECTED) == true && $field->getValue()) {
	            if (! is_object($field->getValue()) || ($field->getValue() instanceof BaseObject && $field->getValue()->getUri())) {
    	        	$p .= $line . '<div class="field">' . $field->formatValue($field->getValue()) . '</div>';
        	    	continue;
            	}
    	    }
            
            /* look for a required decorator */
            if (isset($altDecorators[$element->getId()])) {
            	$element->setDecorator($altDecorators[$element->getId()]);
            }
            
            $deco = View\Decorator::factory($element);
	        $line .= sprintf('<div class="field" id="elem_%s">%s</div>', $element->getId(), $deco->render());
            $p .= $line . "\n";
        }

        $p .= '</fieldset>';
        
        return $p;
    }


    protected function _renderButton(Element\ButtonElement $button)
    {
    	$deco = View\Decorator::factory($button, array('size' => 'medium'));
    	return $deco->render();
    }
    
    
    protected function _headerRendering()
    {
		return  parent::_headerRendering() . $this->_formHeader();
    }

    
    protected function _footerRendering()
    {
    	return $this->_formFooter() . parent::_footerRendering();
    }
    
    
    protected function _formHeader()
    {
		return sprintf('<form id="%s_form" method="post">', $this->_id);
    }
    
    
    protected function _formFooter()
    {
    	/**
    	 * Buttons are displayed by maell.view.form.js
    	 */    	
    	return sprintf('<fieldset class="maell form_actions"></fieldset></form>');
    }
    
    
    public function save($data)
    {
    	return $this->_obj->save($data);
    }
}
