<?php

namespace Maell\View;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\Core\Layout\Menu;


class PathComponent extends ViewObject {
	
	
	/**
	 * 
	 * @var Maell\Core\Layout\Menu
	 */
	protected $_menu;
	
	
	protected $_suffix;
	
	
	/**
	 * 
	 * @param Menu $menu
	 * @return \Maell\View\PathComponent
	 */
	public function setMenu(Menu $menu)
	{
		$this->_menu = $menu;
		return $this;
	}
	
	
	/**
	 * Return the menu
	 * @return Menu
	 */
	public function getMenu()
	{
		return $this->_menu;
	}
	
	
	public function setSuffix($str)
	{
		$this->_suffix = $str;
		return $this;
	}
	
	
	public function getSuffix()
	{
		return $this->_suffix;
	}
}
