<?php

namespace Maell\View\Action;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 * @version    $Revision: 832 $
 */

use Maell\Core;
use Maell\ObjectModel\ObjectUri;
use Maell\Backend\Condition;
use Maell\ObjectModel\Property;
use Maell\ObjectModel;

/**
 * Class providing an AJAX autocompletion controller.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class AutocompleteAction extends AbstractAction {

	
	const SEARCHMODE_CONTAINS = 'contains';
	
	const SEARCHMODE_BEGINS   = 'begins';
	
	
	protected $_id = 'action/autocomplete';
	
	
	protected $_callbacks = array();
	
	
	protected $_context = array('minChars' => 1, 'displayMode' => 'list', 'defaultSelect' => true, 'latency' => 500);
		
	
	/**
	 * Object class
	 *
	 * @var string
	 */
	protected $_objClass = 'Maell\ObjectModel\Collection';
	
	
	protected $_parsedDisplay;
	
	
	protected $_parsedSearchDisplay;
	
	
	public $queryfield = '_query';
	
	
	public $queryidfield	= '_id';
	
	
	/**
	 * Execute the action and returns a result
	 *
	 * @return array
	 */
	public function execute($params = null)
	{
		if ((! isset($params[$this->queryfield]) || empty($params[$this->queryfield]))
		&& (! isset($params[$this->queryidfield]) || empty($params[$this->queryidfield]))) {
			return false;
		}
		
		if (isset($params['_offset'])) {
			$this->setParameter('offset', (int) $params['_offset']);
		}
		
		$extra = isset($params['extra']) ? (array) $params['extra'] : array();
		
		if (\Maell::getEnvData('cache_datasets') === true) {
			$hash  = serialize($extra);
			$hash .= isset($params[$this->queryfield]) ? $params[$this->queryfield] : $params[$this->queryidfield];
			$md5 = md5($hash);
			
			//@todo check unicity, especially with hard-coded conditions having()
			$ckey = 'ds_ac_' . $this->_cachePrefix . '_' . $md5
				  . '_' . $this->getParameter('offset') . '_' . $this->getParameter('batch');
		
			if (($data = \Maell::cacheGet($ckey)) === false) {
				if (isset($params[$this->queryfield])) {
					$data = $this->_getSuggestions(trim($params[$this->queryfield]), $extra);
				} else {
					$data = $this->_getFromIdentifier(trim($params[$this->queryidfield]));
				}
				$data['cache-key'] = $ckey;
				\Maell::cacheSet($data,$ckey, true, array('tags' => array('view','ac')));
			}
			
		} else {
			
			if (isset($params[$this->queryfield])) {
				$data = $this->_getSuggestions(trim($params[$this->queryfield]), $extra);
			} else {
				$data = $this->_getFromIdentifier(trim($params[$this->queryidfield]));
			}
		}
		
		return $data;
	}
	
	
	/**
	 * Execute a find() call on the collection with the current query
	 * @param string $query
	 * @return array
	 */
	protected function _getSuggestions($query, array $extras = array())
	{
		$data = array();
		
		$combo = $this->_obj->setConditionCombo(Condition::MODE_AND);
		foreach ($this->getParameter('searchprops') as $property) {
			
			switch ($this->getParameter('searchmode')) {
				
				case self::SEARCHMODE_BEGINS:
					$combo->having($property)->beginsWith($query);
					break;
					
				case self::SEARCHMODE_CONTAINS:
				default:
					$combo->having($property)->contains($query);
					break;
			}
		}
		
		// if extra parameters are passed, complete query with given key/value pairs
		if (count($extras) > 0) {
			foreach ($extras as $prop => $extra) {
				if (! empty($extra)) {
					$this->_obj->having($prop)->equals($extra);
				}
			}
		}
		
		$this->_obj->setBoundaryOffset($this->getParameter('offset'));
		$this->_obj->setBoundaryBatch($this->getParameter('batch'));

		if ($this->_obj->count() == 0) {
			return false;
		}

		$params = $this->getParameter('member_reduce_params') ? $this->getParameter('member_reduce_params') : array();
		if (! isset($params['props'])) {
		    $params['props'] = array_merge($this->getParameter('display'), $this->getParameter('sdisplay'));
		} else {
		    $params['props'] = array_merge($params['props'],$this->getParameter('display'));
		}		
		foreach ($this->_obj->getMembers() as $member) {
			$data[$member->getUri()->getIdentifier()] = $member->reduce($params);
		}
		
		return array('collection' => $data, 'max' => $this->_obj->getMax(), 'total' => $this->_obj->getTotalMembers());	
	}
	
	
	/**
	 * Execute a find() call on the collection with the current identifier
	 * @param string $query
	 * @return array
	 */
	protected function _getFromIdentifier($id)
	{
		$data = array();
	
		$this->_obj->having(ObjectUri::IDENTIFIER)->equals($id);	
		$this->_obj->setBoundaryOffset(0);
		$this->_obj->setBoundaryBatch(1);
			
		if ($this->_obj->find(ObjectModel::MODEL) === false) {
			return false;
		}
	
		//\Zend_Debug::dump(\Maell\Backend::getLastQuery()); die;
		foreach ($this->_obj->getMembers() as $member) {
			$data[$member->getUri()->getIdentifier()] = $member->reduce((array) $this->getParameter('member_reduce_params'));
		}
	
		return array('collection' => $data, 'max' => $this->_obj->getMax(), 'total' => $this->_obj->getTotalMembers());
	}

	
	public function getDisplay()
	{
		if (! $this->_parsedDisplay) {
		
			// @todo get propertys from objects and collection properties
			$this->_parsedDisplay = array();
			
			/* @var $do Maell\ObjectModel\DataObject */
			$do = $this->_obj->getDataObject();
			
			foreach ($this->getParameter('display') as $propId) {
		
				$property = $do->getRecursiveProperty($propId);
				if (! $property instanceof Property\AbstractProperty) {
					continue;
				}
		
				if (strstr($propId, '.')) {
					$propId = '_' . $propId;
				}
		
				$this->_parsedDisplay[$propId] = $property->reduce();
			}
		}
		
		return $this->_parsedDisplay;
	}
	
	
	public function getSearchDisplay()
	{
		if (! $this->_parsedSearchDisplay) {
	
			// @todo get propertys from objects and collection properties
			$this->_parsedSearchDisplay = array();
				
			/* @var $do Maell\ObjectModel\DataObject */
			$do = $this->_obj->getDataObject();
				
			foreach ($this->getParameter('sdisplay') as $propId) {
	
				$property = $do->getRecursiveProperty($propId);
				if (! $property instanceof Property\AbstractProperty) {
					continue;
				}
	
				if (strstr($propId, '.')) {
					$propId = '_' . $propId;
				}
	
				$this->_parsedSearchDisplay[$propId] = $property->reduce();
			}
		}
	
		return $this->_parsedSearchDisplay;
	}
	
	
	public function reduce(array $params = array())
	{
		$array = parent::reduce($params);
		$array['data']['display'] = $this->getDisplay();
		$array['data']['sdisplay'] = $this->getSearchDisplay();
		
		return $array;
	}
}
