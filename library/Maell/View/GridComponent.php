<?php

namespace Maell\View;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\Core\Registry;
use Maell\ObjectModel;
use Maell\ObjectModel\Property;
use Maell\ObjectModel\ObjectUri;
use Maell\View\ListComponent\Element;
use Maell\View\ListComponent\Element\MetaElement;
use Maell\View\ViewUri\Adapter\AbstractAdapter;

/**
 * Class providing data list objects
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class GridComponent extends ViewObject {

	
	const METACOL = '*';
	
	
	protected $_collection;
	

	protected $_elements;
	

	protected $_events = array('global' => array(), 'row' => array());
	
	
	protected $_columns;
	
	
	protected $_groups = array();
	

	/**
	 * Record aliases array to perform actions on collection
	 * @var array
	 */
	protected $_aliases = array();
	
	
	/**
	 * Class constructor

	 * @param Maell\ObjectModel\Collection $collection
	 * @param array $params
	 */
    public function __construct(ObjectModel\Collection $collection, array $params = null)
    {
    	$this->_collection = $collection;
    	parent::__construct(null, $params);
    }
    

    /**
     * Returns the collection handled by the object
     * @return Maell\ObjectModel\Collection
     */
    public function getCollection()
    {
    	return $this->_collection;
    }
    
    
    /**
     * Define an array of printable columns based on list or setted parameter
     * @return array
     */
    public function getColumns()
    {
    	if (! is_array($this->_columns)) {
    		
    		$alt = $this->getParameter('altlabels');
    		
    		$do = $this->_collection->getDataObject();
    		$columns = $this->getParameter('columns') ? $this->getParameter('columns') : array_keys($do->getProperties());
    		
    		$this->_columns = array();
    		
    		foreach ($columns as $key => $column) {
    			
    			// meta columns are useful to display calculated (not stored) values
    			if (substr($column, 0, 1) == self::METACOL) {
    				$parts = explode(':', substr($column, 1));
    				$obj = new Element\MetaElement($parts[0]);
    				$obj->setParameter('property', $parts[0]);
    				if (isset($parts[1])) $obj->setParameter('action', $parts[1]);
    				$this->_columns[$key] = $obj;
    				continue;
    				
    			} else if ($column instanceof MetaElement) {
    				$this->_columns[$key] = $column;
    				continue;
    			}

    			if ($column == ObjectUri::IDENTIFIER) {
    				$obj = new Element\IdentifierElement();
    				$obj->setTitle(isset($alt[$column]) ? $alt[$column] : 'ID');
    				$this->_columns[$column] = array('type' => 'String', 'value' => isset($alt[$column]) ? $alt[$column] : 'ID');
    			}
    			
    			// $column may contain recursive property reference
    			$parts = explode('.', $column);
    			 
    			// find matching property
    			$property = $do->getRecursiveProperty($column);
    			 
    			if (! $property instanceof Property\AbstractProperty) {
    				continue;
    			}
    			
    			$pclass = get_class($property);
    			
	    		$obj = array('value'	=> isset($alt[$column]) ? $alt[$column] : $property->getLabel()
	    					,'type'		=> str_replace('Property','', substr($pclass, strrpos($pclass,"\\")+1))
	    					);
    						
    			$this->_columns[$column] = $obj;
    		}
    	}
    	
    	return $this->_columns;
    }
    
    
    public function addGroup($name, $from, $to)
    {
    	$this->_groups[] = array('name'		=> $name
    						   , 'from'		=> $from
    						   , 'to' 		=> $to
    	);
    	return $this;
    }
    
    
    public function getGroups()
    {
    	return $this->_groups;
    }
    
    
    /**
     * Execute a query on the collection after injection of the optional parameters
     * extracted from the View Uri adapter environment
     * @param AbstractAdapter $uriAdapter
     */
    public function query(AbstractAdapter $uriAdapter)
    {
    	$offsetIdentifier	= $uriAdapter->getIdentifier('offset');
    	$sortIdentifier		= $uriAdapter->getIdentifier('sort');
    	$searchIdentifier	= $uriAdapter->getIdentifier('search');
    	 
        // try and restore cached search terms for the current uri
        // don't do this if collection has been spawned from an object property
    	if (! $this->_collection->getParent() && $this->getParameter('uricache') != false) {
	    	$uriAdapter->restoreSearchTerms();
    	}
    	
    	$env = $uriAdapter->getEnv();
    	 
    	// set query parameters from context
    	if (isset($env[$searchIdentifier]) && is_array($env[$searchIdentifier])) {
    		foreach ($env[$searchIdentifier] as $field => $value) {
    			$field = str_replace("-",".",$field);
    	
    			if ($value != '' && $value != Property::EMPTY_VALUE) {
    				$property = $this->_collection->getDataObject()->getRecursiveProperty($field);
    				if ($property instanceof Property\MetaProperty) {
    					$this->_collection->having($property->getParameter('property'))->contains($value);
    				} else if ($property instanceof Property\ObjectProperty) {
    					$this->_collection->resetConditions($field);
    					$this->_collection->having($field)->equals($value);
    				} else if ($property instanceof Property\DateProperty) {
    					if (is_array($value)) {
    						if (isset($value['from']) && ! empty($value['from'])) {
    							$this->_collection->having($field)->greaterOrEquals($value['from']);
    						}
    						if (isset($value['to']) && ! empty($value['to'])) {
    							$this->_collection->having($field)->lowerOrEquals($value['to']);
    						}
    					} else {
    						$this->_collection->having($field)->equals($value);
    					}
    				} else if ($property instanceof Property\IntegerProperty) {
    					$this->_collection->resetConditions($field);
    					$this->_collection->having($field)->equals($value);
    				} else if ($property instanceof Property\AbstractProperty) {
    					$this->_collection->resetConditions($field);
    					$this->_collection->having($field)->contains($value);
    				}
    				$uriAdapter->setArgument($searchIdentifier . '[' . $field . ']', $value);
    			}
    		}
    	}
    	 
    	// set query sorting from context
    	if (isset($env[$sortIdentifier]) && is_array($env[$sortIdentifier])) {
    		foreach ($env[$sortIdentifier] as $field => $value) {
    			$this->_collection->setSorting(array($field, $value));
    		}
    	}
    	
    	// define offset parameter value from context
    	if (isset($env[$offsetIdentifier])) {
    		$this->setParameter('offset', (int) $env[$offsetIdentifier]);
    		$this->_collection->setBoundaryOffset($env[$offsetIdentifier]);
    	} else {
    		$this->_collection->setBoundaryOffset(-1);
    	}
    	
    	$this->_collection->find(ObjectModel::DATA);
    	$this->setParameter('max', $this->_collection->getMax());
    	$this->bindAliases();
    }
    
    
    /**
     * Create and array of members identifiers & md5 hash in order to securely
     * manipulate members in view
     */
    public function bindAliases()
    {
    	$this->_aliases = array();
    	foreach ($this->_collection->getMembers() as $member) {
    		$this->_aliases[$member->getIdentifier()] = md5(microtime());
    	}
    	return $this->_aliases;
    }
    
    
    public function getAliases()
    {
    	if (count($this->_aliases) == 0) {
    		$this->bindAliases();
    	}
    	return $this->_aliases;
    }
    
    /**
     * Return the matching member identifier of the given md5 alias if exists
     * false otherwise
     * @param string $alias
     * @return mixed
     */
    public function getIdentifierFromAlias($alias)
    {
    	return array_search($alias, $this->_aliases);
    }
    
    
    /**
     * 
     * @param string $type
     * @param Element\ButtonElement|string $button
     * @param array $params Decorator parameters
     * @throws Exception
     */
    public function addRowAction($link, $label, array $params = null)
    {
    	$event = array('label' => $label, 'callback' => $link, 'options' => $params);
    	return $this->_addEvent('row', $event);
    }
    
    
    protected function _addEvent($scope, $button = null)
    {
    	if (! is_array($this->_events[$scope])) {
    		$this->_events[$scope] = array();
    	}
    	$this->_events[$scope][] = $button;
    	return $this;
    }
    
    
    public function getEvents($scope = null)
    {
    	return isset($this->_events[$scope]) ? $this->_events[$scope] : $this->_events;
    }
    
    
    public function reduce(array $params = array(), $cache = true)
    {
    	$uuid = Registry::set($this, null, true);
    	return array_merge(parent::reduce($params), 
    			array(	'uuid'		=> $uuid, 
    					'obj'		=> $this->_collection->reduce($params, $cache), 
    					'display'	=> $this->getColumns(),
    					'events'	=> $this->getEvents()
    				 )
    	);
    }
}
