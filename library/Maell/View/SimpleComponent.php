<?php

namespace Maell\View;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\Core\Parameter;

/**
 * Class providing methods and parameters to component objects.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class SimpleComponent extends ViewObject {
	
	public function __construct($id = null, array $params = null)
	{
		$this->_setParameterObjects(array(	
								'open_default'		=> new Parameter(\Maell\Parameter::BOOLEAN, true),
								'columns'			=> new Parameter(\Maell\Parameter::BOOLEAN, false),
								'locked'			=> new Parameter(\Maell\Parameter::BOOLEAN, false)
							  ));
		parent::__construct($id, $params);
	}
	
	
	/**
	 * Add a template component to the current object content
	 * @param string $template
	 * @return SimpleComponent
	 */
	public function setTemplate($template)
	{
		$tpl = new TemplateComponent();
		$tpl->load($template);
		$this->_content[] = $tpl;
		return $this;
	}
	
	
	/**
	 * Returns the number of lines depending on Parameter 'columns'
	 * which splits in two columns
	 *
	 * @return int
	 */
	public function lines($floor=true)
	{
		$cols = count($this->_columns);
		$round = $floor ? 'floor' : 'ceil';
		
		if ($this->getParameter('columns')) {
			$lines = ($cols % 2 == 0) ? $cols / 2 : $round($cols / 2);
		} else {
			$lines = $cols;
		}
		
		return $lines;
	}
}
