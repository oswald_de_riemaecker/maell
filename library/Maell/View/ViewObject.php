<?php

namespace Maell\View;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\View;
use Maell\ObjectModel\ObjectModelAbstract;

/**
 * class providing basic methods to every view-related object.
 *
 * @category   maell
 * @package    Maell_View
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
abstract class ViewObject extends ObjectModelAbstract {
	
	
	protected $_title;
	
	
	protected $_parent;
	
	
	protected $_help;
	
	
	protected $_content = array();
	
	
	protected $_decorator = array('name' => 'default');
	
	
	/**
	 * Latest call on object's status
	 * @var Maell\Core\Status
	 */
	public $status;
		
	
	/**
	 * Set object title value
	 * @param string $str
	 * @return \Maell\View\ViewObject
	 */
	public function setTitle($str)
	{
		$this->_title = $str;
		return $this;
	}
	
	
	/**
	 * Return object's title current value
	 * @return string
	 */
	public function getTitle()
	{
		return $this->_title;
	}
	
	
	/**
	 * 
	 * Add content to the view object
	 * @param mixed $str
	 * @return $this
	 */
	public function setContent($str = null)
	{
		$this->_content[] = $str;
		return $this;
	}
	
	
	/**
	 * Get registered content
	 * @return multitype:
	 */
	public function getContent()
	{
		return $this->_content;
	}
	

	/**
	 * Register the object's parent
	 * @param ViewObject $parent
	 * @return \Maell\View\ViewObject
	 */
	public function setParent(ViewObject $parent)
	{
		$this->_parent = $parent;
		return $this;
	}
	
	
	public function getParent()
	{
		return $this->_parent;
	}
	
	
	/**
	 * Sets a help text message
	 * @param string $str
	 */
	public function setHelp($str)
	{
		$this->_help = $str;
		return $this;
	}
	
	
	/**
	 * Get help text
	 * @return string
	 */
	public function getHelp()
	{
		return $this->_help;
	}
	
	/**
	 * 
	 * Register the object in the view within the given placeholder and optional decorator parameters
	 * if top is true, object is added at the beginning of the list
	 * @param string $placeHolder
	 * @param array $params
	 * @param boolean $clone
	 * @param boolean $top
	 * @return boolean
	 */
	public function register($placeHolder = View::PH_DEFAULT, array $params = null, $clone = false, $top = false)
	{
		$obj = $clone ? clone $this : $this;
		return View::addObject($obj, $placeHolder, $params, $top);
	}
	
	
	/**
	 * Set which decorator to use with object
	 *
	 * @param string $decorator Decorator name. 
	 * 							If relative, will be resolved as {objectClass}\{ViewType}{DecoratorName}
	 * 							If value begins with a namespace separator, matching class will be called directly
	 * @param array $params decorator parameters
	 * @return Maell\View\ViewObject
	 */
	final public function setDecorator($decorator = 'Default', array $params = array())
	{
		if (is_array($decorator)) {
			$this->_decorator = $decorator;
		} else {
			$this->_decorator['name']	= $decorator;
			$this->_decorator['params'] = $params;
		}
		return $this;
	}
	
	
	/**
	 * Shortcut to define decorator parameters
	 * @param array $array
	 */
	public function setDecoratorParams(array $array)
	{
		$this->_decorator['params'] = $array;
		return $this;
	}
	
	
	/**
	 * Retrieve parameters that the selected decorator will receive upon instanciation
	 * @return array
	 */
	public function getDecoratorParams()
	{
		return isset($this->_decorator['params']) ? $this->_decorator['params'] : array();
	}
	
	
	public function getDecorator()
	{
		return $this->_decorator;
	}
}
