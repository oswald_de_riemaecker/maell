<?php

namespace Maell;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

use Maell\ObjectModel;
use Maell\ObjectModel\Property;
use Maell\View;

/**
 * Class providing objects parameters wrapper ensuring basic logic control.
 *
 * @category   maell
 * @package    Maell
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Parameter {

	
	const BOOLEAN	= 'boolean';
	
	const INTEGER	= 'integer';
	
	const FLOAT		= 'float';
	
	const STRING	= 'string';
	
	const MULTIPLE	= 'array';
	
	const ANY		= 'any';
	
	const OBJECT	= 'object';
	
	
	/**
	 * Array of various objects parameters definitions
	 * 
	 * @var array
	 */
	static protected $_config = array();
	
	
	/**
	 * Load a configuration file (default value is objects.xml) and add or replace content
	 * 
	 * @param string $file name of file to parse, file should be in application/configs folder
	 * @param boolean $add wether to add to (true) or replace (false) existing configuration data
	 * @return boolean true in case of success, false otherwise
	 */
	static public function loadConfig($file, $add = true)
	{
		$config = Config\Loader::loadConfig($file);

		if ($config === false) {
			return false;
		}
		
		if ($add === false) {
			self::$_config = $config;
		} else {
	        self::$_config = array_merge(self::$_config, $config);
		}
		return true;
	}
	
	
	static protected function _cloneParametersArray($array)
	{
		foreach ($array as $key => $parameter) {
			$array[$key] = clone $parameter;
		}
		return $array;
	}

	
	static public function getParameters($object)
	{
		$class = get_class($object);
		
		if ($object instanceof ObjectModel\BaseObject) {
			$params = self::getObjectParameters($class);
			
		} else if ($object instanceof Property\AbstractProperty) {
			$params = self::getPropertyParameters($class);
		
		} else if ($object instanceof View\ViewObject || $object instanceof View\Action\AbstractAction) {
			$params = self::getViewObjectParameters($class);
		
		} else if ($object instanceof View\Decorator\AbstractDecorator) {
			$params =  self::getDecoratorParameters($class);
		}
		
		if (isset($params)) {
			return $params;
		} else if ($object instanceof ObjectModel\ObjectModelAbstract) {
			return self::getCoreParameters($class);
		}
	}
	
	
	static public function getCoreParameters($objectClass)
	{
		if (! isset(self::$_config['core'])) {
			self::loadConfig('parameters/core.xml');
		}
		$array = self::_compileFragments($objectClass, 'core');

		// transform each array value into a Core\Parameter object
		return (count($array) != 0) ? self::_arrayToParameters($array) : $array;
	}
	
	
	static public function getObjectParameters($objectClass)
	{
		if (! isset(self::$_config['objects'])) {
			self::loadConfig('objects.xml');
		}
		$array = self::_compileFragments($objectClass);
		// transform each array value into a Core\Parameter object
		return (count($array) != 0) ? self::_arrayToParameters($array) : $array;
	}
	
	
	static public function getPropertyParameters($objectClass)
	{
		if (! isset(self::$_config['properties'])) {
			self::loadConfig('parameters/properties.xml');
		}

		$array = self::_compileFragments($objectClass, 'properties');
		// transform each array value into a Core\Parameter object
		return (count($array) != 0) ? self::_arrayToParameters($array) : $array;
	}
	
	
	static public function getViewObjectParameters($objectClass)
	{
		if (! isset(self::$_config['view'])) {
			self::loadConfig('parameters/view/objects.xml');
		}
		$array = self::_compileFragments($objectClass, 'view');
		return (count($array) != 0) ? self::_arrayToParameters($array) : $array;
	}
	
	
	static public function getDecoratorParameters($objectClass)
	{
		$elems = explode('\\', $objectClass);
		
		$sublevel = $elems[count($elems)-1];
		unset($elems[count($elems)-1]);
		
		$class = implode('\\', $elems);
		
		if (! isset(self::$_config['decorators'])) {
			self::loadConfig('parameters/view/decorators.xml');
		}
		$array = self::_compileFragments($class, 'decorators', $sublevel);
		return (count($array) != 0) ? self::_arrayToParameters($array) : $array;		
	}
	
	
	/**
	 * Compile fragments of xml configuration 
	 * 
	 * @param string $objectClass
	 * @param string $objectType
	 * @param string $subLevel		sublevel path where data should exist (ex: web/default for a default web decorator)
	 * @return array
	 */
	static protected function _compileFragments($objectClass, $objectType = 'objects', $subLevel = null)
	{
		$array = array();
		if (isset(self::$_config[$objectType][$objectClass])) {
			$sub = self::$_config[$objectType][$objectClass];
			
			if ($subLevel) {
				$levels = explode('/', $subLevel);
				foreach ($levels as $level) {
					if (isset($sub[$level])) {
						$sub = $sub[$level];
					} else {
						return $array;
					}
				}

			}
			if (isset($sub['parameters']) && is_array($sub['parameters'])) {
				$array += $sub['parameters'];
			}
			
			/* if class extends another, get parent parameters */
			if (isset($sub['extends']) && ! empty($sub['extends'])) {
				$array += self::_compileFragments($sub['extends'], $objectType, $subLevel);
			}
		}
		
		return $array;
	}
	
	
	static protected function _arrayToParameters(array $array)
	{
		foreach ($array as $key => $value) {
			/* ignore parameter without any given type */
			if (! isset($value['type']) || empty($value['type'])) continue;
				
			$array[$key] = new \Maell\Core\Parameter($value['type']
								  , isset($value['defaultvalue']) ? $value['defaultvalue'] : null
								  , isset($value['protected']) ? (bool) $value['protected'] : false
								  , isset($value['values']) ? $value['values'] : null
								   );
		}
		return $array;
	}
}
