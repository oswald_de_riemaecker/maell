<?php

namespace Maell;

/**
 * Maell Toolkit
 *
 * LICENSE
 *
 * This source file is subject to the new BSD license that is bundled
 * with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.maell.org/license/new-bsd
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@maell.org so we can send you a copy immediately.
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */

/**
 * Class providing basic functions needed to handle data mapping
 *
 * @category   maell
 * @package    Maell_Core
 * @copyright  Copyright (c) 2014 Olivier Lépine
 * @license    http://www.maell.org/license/new-bsd     New BSD License
 */
class Mapper {
	
	
	/**
	 * Array of mappers definitions
	 * 
	 * @var array
	 */
	static protected $_config = array();
	

	/**
	 * Mappers objects instances store
	 * 
	 * @var array
	 */
	static protected $_instances = array('backend' => array());
	
	
	/**
	 * Load a configuration file and add or replace content
	 * 
	 * @param string $file name of file to parse, file should be in application/configs folder
	 * @param boolean $add wether to add to (true) or replace (false) existing configuration data
	 * @return boolean true in case of success, false otherwise
	 */
	static public function loadConfig($file = 'mappers.xml', $add = true)
	{
		$config = Config\Loader::loadConfig($file);
		if ($config === false) {
			return false;
		}
		
		if ($add === false) {
			self::$_config = $config['mappers'];
		} else {
	        self::$_config = array_merge(self::$_config, $config['mappers']);
		}
		return true;
	}
	
	
	static public function factory($id)
	{
		if (! array_key_exists($id, self::$_config)) {
			throw new ObjectModel\Exception(array('MAPPER_NO_DECLARATION', $id));
		}
		
		try {
			$obj = new Backend\Mapper(self::$_config[$id]);
		} catch (ObjectModel\Exception $e) {
			
		} catch (ObjectModel\DataObject\Exception $e) {
			
		}
		
		return $obj;
	}
	

	static public function getInstance($key, $type = 'backend')
	{
		if (isset(self::$_instances['backend'][$key])) {
			return self::$_instances['backend'][$key];
		} else {
			$mapper = self::factory($key);
			self::$_instances['backend'][$key] = $mapper;
			return $mapper;
		}
	}
}
